<div class="content">
	<div id="thm-mart-slideshow" class="thm-mart-slideshow">
    	<div id='thm_slider_wrapper' class='thm_slider_wrapper fullwidthbanner-container'>
        	<div id='thm-rev-slider' class='rev_slider fullwidthabanner'>
            	<ul>
                <?php
				//for( $i = 1; $i <= 4; $i++ ) {		
					
				?>
                	<li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='theme/imgs/banner_imgs/banner-2.jpg'><img src='theme/imgs/banner_imgs/banner-1.jpg' data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="slider-image1" />
                    	<div class="info">
                        	<div class="container">
                            	<div class='tp-caption ExtraLargeTitle sft tp-resizeme col-sm-6 col-sm-push-3 col-sm-pull-3' data-x='-25%' data-y='500' data-endspeed='500' data-speed='500' data-start='-500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' style="z-index:2" >
                            </div>
                        </div>
                    </li>
                    <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='theme/imgs/banner_imgs/banner-3.jpg'><img src='theme/imgs/banner_imgs/banner-3.jpg'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="slider-image1" />
                    	<div class="info">
                        	<div class="container">
                            	<div class='tp-caption ExtraLargeTitle sft tp-resizeme col-sm-6 col-sm-push-3 col-sm-pull-3' data-x='-25%'  data-y='500'  data-endspeed='500'  data-speed='500'  data-start='-500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' style="z-index:2" >
                            </div>
                        </div>
                    </li>
                    <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='theme/imgs/banner_imgs/banner-4.jpg'><img src='theme/imgs/banner_imgs/banner-4.jpg'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="slider-image1" />
                    	<div class="info">
                        	<div class="container">
                            	<div class='tp-caption ExtraLargeTitle sft tp-resizeme col-sm-6 col-sm-push-3 col-sm-pull-3' data-x='-25%'  data-y='500'  data-endspeed='500'  data-speed='500'  data-start='-500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' style="z-index:2" >
                            </div>
                        </div>
                    </li>
                <?php
				//}
				?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div style="clear:both !important"></div>