<section class="main-container bounceInUp animated">
	<div class="row">
    	<div class="product-grid col-sm-12">
        	<div class="category-products pro-coloumn">
            	
                <div class="col-md-4 single-right-left pl">
                	<div class="grid images_3_of_2">
                	<div class="flexslider">
                    
                    	<div class="flex-viewport" style="overflow: hidden; position: relative;">
                        	<ul class="slides">
                            <?php
							//for( $i = 1; $i <= 4; $i++ ) {
								?>
                                <li data-thumb="theme/imgs/common_imgs/banner-4.jpg">
                                	<div class="thumb-image">
                                    	<img src="theme/imgs/common_imgs/banner-4.jpg" data-imagezoom="true" class="img-responsive">
                                    </div>
                                </li>
                                <li data-thumb="theme/imgs/common_imgs/banner-4.jpg">
                                	<div class="thumb-image">
                                    	<img src="theme/imgs/common_imgs/banner-4.jpg" data-imagezoom="true" class="img-responsive">
                                    </div>
                                </li>
                                <li data-thumb="theme/imgs/common_imgs/banner-4.jpg">
                                	<div class="thumb-image">
                                    	<img src="theme/imgs/common_imgs/banner-4.jpg" data-imagezoom="true" class="img-responsive">
                                    </div>
                                </li>
                            	<?php
							//}
							?>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <ol class="flex-control-nav flex-control-thumbs">
                        	<li><img src="theme/imgs/common_imgs/banner-4.jpg" class="" draggable="false"></li>
                            <li><img src="theme/imgs/common_imgs/banner-4.jpg" draggable="false" class="flex-active"></li>
                            <li><img src="theme/imgs/common_imgs/banner-4.jpg" draggable="false" class=""></li>
                        </ol>
                    </div>
                    </div>
                </div>
                
                <div class="col-md-8 single-right-left simpleCart_shelfItem pl">
                	<h3>Apple</h3>
                    <p>
                    	<span class="item_price">$650</span> 
                    	<del>- $900</del>
                    </p>
                    <div class="col-sm-12 row">
                    	<div class="col-sm-2 pl">
                    		<div class="incr-decr quantity">
                    			<div class="btn-increment-decrement" onClick="decrement_quantity(<?php echo($i); ?>,'<?php //echo($rate); ?>')">-</div>
                        		<input class="input-quantity" id="input-quantity-<?php echo($i); ?>" value="1<?php //echo $item["quantity"]; ?>" size="2">
                        		<div class="btn-increment-decrement" onClick="increment_quantity(<?php echo($i); ?>,'<?php //echo($rate); ?>')">+</div>
                        	</div>
                        </div>
                        <div class="col-sm-4 pl">
                        	<select name="variat" class="form-control" style="width:auto; height:25px; padding:0px;">
                            	<option value="">250 Gm</option>
                            </select>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
