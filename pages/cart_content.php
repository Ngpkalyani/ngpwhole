<?php
$clientID = "";
if( isset($_SESSION[$sessName]) ) {
	$clientID = $_SESSION[$sessName][0]['clients_id'];
}

$cond = "clients_id='$clientID'";
$order = "";
$limit = "1";
$clientData = $Client->select($Client->table, '', $cond, $order, $limit);

$client_name = "";
foreach( $clientData as $clientValue ) {
	$client_name = $clientValue['client_name'];
	$contact_no = $clientValue['contact_no'];
	$email_id = $clientValue['email_id'];
	$address = $clientValue['address'];
	$city = $clientValue['city'];
	$pincode = $clientValue['pincode'];
	$gst_no = $clientValue['gst_no'];
}
?>
<section class="main-container bounceInUp animated">
	<div class="container">
    	<div class="product-grid col-sm-12">
        	<div class="category-products pro-coloumn">
            
            <form name="onlineOrderFrm" method="post" action="" enctype="multipart/form-data">
            <div class="row">
            	<div class="form-group col-sm-12 row">
                	<div class="col-sm-3 pl">
                    	<label class="control-label">Full Name *</label>
                        <input type="text" name="customerName" value="<?php echo isset($client_name) ? $client_name : ""; ?>" required placeholder="Full Name" readonly class="form-control" />
                    </div>
                    <div class="col-sm-3 pl">
                    	<label class="control-label">Mobile No *</label>
                        <input type="text" name="mobileNo" value="<?php echo isset($contact_no) ? $contact_no : ""; ?>" required placeholder="Mobile No" readonly class="form-control" />
                    </div>
                    <div class="col-sm-3 pl">
                    	<label class="control-label">Email ID *</label>
                        <?php $readonly_email = !empty($email_id) ? "readonly" : "";  ?>
                        <input type="text" name="emailID" value="<?php echo isset($email_id) ? $email_id : ""; ?>" required placeholder="Email ID" <?php echo $readonly_email; ?> class="form-control" />
                    </div>
                    <div class="col-sm-3 pl">
                    	<label class="control-label">GST No (If Applicable)</label>
                        <?php $readonly_gstNo = !empty($gst_no) ? "readonly" : "";  ?>
                        <input type="text" name="gstNo" value="<?php echo isset($gst_no) ? $gst_no : ""; ?>" placeholder="GST No." class="form-control" <?php echo $readonly_gstNo; ?> />
                    </div>
				</div>
                
                <div class="form-group col-sm-12 row">
                	<div class="col-sm-3 pl">
                    	<label class="control-label">Address *</label>
                        <textarea name="address" class="form-control" placeholder="Address" required><?php echo $address; ?></textarea>
                    </div>
                    <div class="col-sm-3 pl">
                    	<label class="control-label">City Name *</label>
                        <?php $readonly_city = !empty($city) ? "readonly" : "";  ?>
                        <input type="text" name="cityName" value="<?php echo isset($city) ? $city : ""; ?>" required placeholder="City" <?php echo $readonly_city; ?> class="form-control" />
                    </div>
                    <div class="col-sm-3 pl">
                    	<label class="control-label">Pincode *</label>
                        <input type="text" name="pincode" id="pincode" onblur="getShippingCharges()" onkeydown="getShippingCharges()" value="<?php echo isset($pincode) ? $pincode : ""; ?>" required placeholder="Pincode" max="6" maxlength="6" class="form-control" /> &nbsp;
                        <div class="pincodeErr-msg"></div>
                    </div>
				</div>
                
            </div>
            
            <div class="row">
            <?php
			if( isset($_COOKIE["shopping_cart"]) )
			{
				$total = 0;
				$cookie_data = stripslashes($_COOKIE['shopping_cart']);
				$cart_data = json_decode($cookie_data, true);
				?>
                <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped order-table">
                	<thead>
                    	<tr class="text-center">
                        	<th>SN</th>
                            <th>Product Name</th>
                            <th>Variant</th>
                            <th>QTY</th>
                            <th>Price</th>
                            <th>AMT</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$sr_no = 1;
					foreach( $cart_data as $keys => $values ) {
						?>
                    	<tr>
                        	<td class="text-center"><?php echo($sr_no); ?></td>
                            <td>
								<?php echo $values["item_name"]; ?>
                                <input type="hidden" name="productCode[]" value="<?php echo $values["item_id"]; ?>"  />
                                <input type="hidden" name="productName[]" value="<?php echo $values["item_name"]; ?>"  />
                            </td>
                            <td>
								<?php 
								$item_variantName = trim($values["item_variantName"]);								
								$variant_name = explode(" ", $item_variantName);
								array_splice($variant_name, -1);
								$variantName = implode(" ", $variant_name);
								echo $variantName = trim($variantName);
								?>
                                <input type="hidden" name="variantName[]" value="<?php echo $variantName; ?>" />
                            </td>
                            <td class="text-center">
								<?php echo $values["item_quantity"]; ?>
                                <input type="hidden" name="quantity[]" value="<?php echo $values["item_quantity"]; ?>"  />
                            </td>
                            <td class="text-right">
								<?php echo $values["item_price"]; ?>
                                <input type="hidden" name="price[]" value="<?php echo $values["item_price"]; ?>"  />
                            </td>
                            <td class="text-right"><?php echo number_format($values["item_quantity"] * $values["item_price"], 2);?></span></td>
                            <td class="text-center"><a onclick="addToCart('remove','<?php echo $values["item_id"]; ?>')"><span class="btn-delete"><i class="fa fa-trash-alt"></i></a></td>
                        </tr>
                        <?php
						$total = $total + ($values["item_quantity"] * $values["item_price"]);
						$sr_no++;
					}
					?>						
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<th colspan="3"></th>
                            <th class="text-center"><?php echo($sr_no-1); ?></th>
                            <th class="text-right">Sub Total :</th>
                            <th class="text-right">
								<?php echo number_format($total, 2); ?>
                                <input type="hidden" name="subTotal" id="subTotal" value="<?php echo $total; ?>" />
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                        <tr>
                        	<th colspan="3"></th>
                            <th colspan="2" class="text-right">Shipping Charges :</th>
                            <th class="text-right">
                            	<div id="shippingCharges"></div>
                                <input type="hidden" name="shippingCharges" id="shippingChargesValue" value="" />
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                        <tr>
                        	<th colspan="3"></th>
                            <th colspan="2" class="text-right">Grand Total :</span></th>
                            <th class="text-right"><div id="finalTotal"><?php echo number_format($total, 2); ?></div></th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                </div>
                <?php
			}
			?>
            </div>
            
            <div class="row">
            	<div class="form-group col-sm-12 row">
                	<div class="col-sm-3 pl">
                    <?php
					date_default_timezone_set('Asia/Calcutta');
					$currentTime = date('g:i');
					$currentAMPM = date('A');
					//echo $currentTime;
					
					$cond = "status>'0'";
					$order = "";
					$delivery_slot_list = $Delivery_slot->select($Delivery_slot->table,'', $cond, $order);
					?>
                    	<label class="control-label">Delivery Slots *</label><br />
                       	<select name="deliverySlots" class="custom-select" required>
                        	<option value=""></option>
                        	<optgroup label="Today">
                            <?php
							foreach( $delivery_slot_list as $deliverySlot_today) {
								$deli_slot = $deliverySlot_today['delivery_slots'];
								$deli_slot = explode('-', $deli_slot);
								$fromTime = trim($deli_slot[0]);
								$toTime = trim($deli_slot[1]);
								$startTime = trim(substr($fromTime, 0, 2)).":00";
								$endTime = trim(substr($toTime, 0, 2)).":00";
								
								//if(  ) {
							?>
                            	<option value="<?php //echo $deli_slot; ?>"><?php //echo $deli_slot; ?></option>
                            <?php
								//}
							}
							?>
                            </optgroup>
                            <optgroup label="Tomorrow">
                            <?php
							foreach( $delivery_slot_list as $deliverSlot_tomorrow ) {
							?>
                            	<option value="<?php echo $deliverSlot_tomorrow['delivery_slots']." - ND"; ?>"><?php echo $deliverSlot_tomorrow['delivery_slots']; ?></option>
                            <?php
							}							
							?>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-sm-3 pl">
                    	<label class="control-label">Payment Mode *</label><br />
                        <label><input type="radio" name="paymentMode" value="COD" onclick="getShippingCharges()" required /> &nbsp; Cash On Delivery</label>
                    </div>
                    <div class="col-sm-3 pr">
                    </div>
                    <div class="col-sm-3 pr">
                    	<input type="hidden" name="custNo" value="<?php echo($clientID); ?>" />
                    	<span class="align-bottom"><input type="submit" name="placeOrder" value="PLACE ORDER" class="btn btn-success" /></span>
                    </div>                    
				</div>
            </div>
            </form>
            
            </div>
        </div>
    </div>
</section>
