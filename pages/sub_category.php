<section class="bounceInUp animated">
	<div class="featured-pro">
    	<div class="row">
        	<div class="slider-items-products">
            	<div class="slider-items slider-width-col4 products-grid owl-carousel owl-theme" style="opacity: 1; display: block;">
                <?php
				foreach( $category_list as $category ) {
					if( $category['status'] > 0 && $category['parent_id'] != 0 ) {
					$imgs = $category['imgs'];
					$catg_img = '<img src="theme/imgs/category_imgs/'.$imgs.'" />';
					$logo_img = '<img src="theme/imgs/common_imgs/nw_logo.png" />';					
					$categoryImage = $imgs != "" ? $catg_img : $logo_img;	
				?>
                	<div class="owl-item" style="width:140px;">
                    	<div class="item">
                        	<div class="pro-img">
                            	<a href="category_result.php"><?php echo $categoryImage; ?></a>
                                <div class="pro-info"><?php echo $category['category_name']; ?></div>
                            </div>
                        </div>
                    </div>
                 <?php
					}
				}
				?>
                </div>
            </div>
        </div>
    </div>
</section>