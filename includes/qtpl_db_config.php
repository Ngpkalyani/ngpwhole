<?php
class QTPLDBConfig
{
	/*// database hostname
	protected $host = 'localhost';
	// database username
	protected $user = 'root';
	// database password
	protected $pass = '';
	//database name
	protected $dbName = 'ngpwholesale';*/
	
	// database hostname 
	protected static $host = "localhost";
	// database username
	protected static $user = "qtplindoreuser";
	// database password
	protected static $pass = "qtpl<indor>wholesale";
	// database name
	protected static $dbName = "qtplindorewholesaleqtpl";
	
	// database hostname 
	//protected static $host = "localhost";
	// database username
	//protected static $user = "qtplngpUser";
	// database password
	//protected static $pass = "HelloQtplNw";
	// database name
	//protected static $dbName = "qtplngpwholesale";
	
	protected $conn;
	
	function __construct()
	{
		 $this->conn = self::db_conn();
	}
	
	// open connection
	protected static function db_conn()
	{
		try {
			$link = mysqli_connect(self::$host, self::$user, self::$pass, self::$dbName);
			if( !$link ) {
				throw new exception(mysqli_error($link));				
			}
			/*echo '<script>alert("Database Connected");</script>';*/			
			return $link;
		}
		catch(Exception $e) {
			echo '<script>alert("Database Not Connected");</script>';	
			//echo $e->getMessage();		
		}
	}
	
	// close connection
	public static function db_close() 
	{
		mysqli_close($this->conn);
	}
	
	public function save($table, $fields, $condition = '')
	{
		$sql = "INSERT INTO $table SET ";
		
		if( $condition != '' ) {
			$sql = "UPDATE $table SET ";
		}
		$table_fields = $this->get_table_fields($table);
		foreach( $fields as $field=>$value ) {
			if( in_array($field, $table_fields) ) {
				$sql .= "$field = '".addslashes($value)."', ";
			}
		}
		
		$sql.="created=NOW(), modified=NOW(), ";
		
		$sql = substr($sql, 0 ,-2);
		if( $condition != '' ) {
			$sql .= " WHERE $condition";
		}
		
		$res = mysqli_query($this->conn, $sql);	
		if( $res ) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public function delete($table, $cond)
	{
		$sql = "DELETE * FROM $table WHERE $cond";
		$res = mysqli_query($sql);
		if( $res ) {
			return true;
		}
		else {
			return false;
		}
	}
  
	
	public function select($table, $fields = array(), $cond = '', $order = '', $limit = '')
	{
		$data = array();
		$sql = "SELECT ";
		
		if( is_array($fields) && count($fields) > 0 ) {
			$sql .= implode(", ",$fields);
		}
		else {
			$sql .= "*";
		}
		
		$sql .= " FROM $table";
		
		if( $cond != '' ) {
			$sql .= " WHERE $cond";
		}
		if( $order != '' ) {
			$sql .= " ORDER BY $order";
		}
		if( $limit != '' ) {
			$sql .= "LIMIT $limit";
		}
		
		$res = mysqli_query($this->conn, $sql);
		
		while( $row = mysqli_fetch_array($res) ) {
			$data[] = $row;
		}
		
		return $data;
	}
	
	public function run($sql)
	{
		try {
			if( empty($sql) && !isset($sql) ) {
				throw new exception("Query string is not set.");
			}
			$res = mysqli_query($this->conn, $sql);
			return $res;
		}
		catch(Exception $e) {
			echo "Error: ".$e->getMessage();
		}
	}
	
	public function get_table_fields($table)
	{
		$fields = array();
		$sql = "SHOW COLUMNS FROM $table";
		$res = mysqli_query($this->conn, $sql);
		while( $row = mysqli_fetch_array($res) ) {
			$fields[] = $row['Field'];
		}
		
		return $fields;
	}
	
}
$QTPLDBConfig = new QTPLDBConfig();
?>