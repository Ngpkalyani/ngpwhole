<?php
require_once('QTPLNW-backend/modules/controllers/controller_user.php');
require_once('QTPLNW-backend/modules/controllers/controller_category.php');
require_once('QTPLNW-backend/modules/controllers/controller_brand.php');
require_once('QTPLNW-backend/modules/controllers/controller_vendor.php');
require_once('QTPLNW-backend/modules/controllers/controller_banner.php');
require_once('QTPLNW-backend/modules/controllers/controller_product.php');
require_once('QTPLNW-backend/modules/controllers/controller_client.php');
require_once('QTPLNW-backend/modules/controllers/controller_delivery_slot.php');
require_once('QTPLNW-backend/modules/controllers/controller_shipping_charge.php');
require_once('QTPLNW-backend/modules/controllers/controller_order.php');
require_once('QTPLNW-backend/modules/controllers/controller_section.php');


?>