<?php

require_once('includes/qtpl_db_config.php');

$sessName = "client_login";
session_start();
session_name($sessName);

$baseUrl = "http://indorewholesale.com/";

require_once('includes/qtpl_models.php');
require_once('includes/qtpl_controllers.php');
require_once('includes/qtpl_objects.php');
?>