<?php
function __autoload($class) {
	/* All Models */
	require_once('QTPLNW-backend/modules/models/model_user.php');
	require_once('QTPLNW-backend/modules/models/model_designation.php');
	require_once('QTPLNW-backend/modules/models/model_category.php');
	require_once('QTPLNW-backend/modules/models/model_brand.php');
	require_once('QTPLNW-backend/modules/models/model_ledgers_groups.php');
	require_once('QTPLNW-backend/modules/models/model_ledgers.php');
	require_once('QTPLNW-backend/modules/models/model_vendor.php');
	require_once('QTPLNW-backend/modules/models/model_measures.php');
	require_once('QTPLNW-backend/modules/models/model_banners.php');
	require_once('QTPLNW-backend/modules/models/model_product.php');
	require_once('QTPLNW-backend/modules/models/model_client.php');
	require_once('QTPLNW-backend/modules/models/model_wallet.php');
	require_once('QTPLNW-backend/modules/models/model_delivery_slot.php');
	require_once('QTPLNW-backend/modules/models/model_shipping_charge.php');
	require_once('QTPLNW-backend/modules/models/model_order.php');
	require_once('QTPLNW-backend/modules/models/model_section.php');
	require_once('QTPLNW-backend/modules/models/model_variant.php');	
}
?>