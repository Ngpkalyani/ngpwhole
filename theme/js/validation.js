// JavaScript Document

function valid_categoryForm()
{
	var catgFormData = document.getElementById("formAddNewCategory");
	var categoryType = $('input[name="categoryType"]:checked', catgFormData).val();
	var parentCategoryName = $('.parentCategoryName');

		if( !categoryType ) {
			alert("Please Select Category Type");
			return false;
		}
		if( categoryType == 'C' ) {
			if( !parentCategoryName.val() ) {				
				alert("Please Select Parent Category");
				$('.parentCategoryName').focus();
				return false;
			}
			return true;
		}
}