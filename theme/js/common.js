// JavaScript Document
// Seaching Reports
(function(document) {
	'use strict';

	var LightTableFilter = (function(Arr) {

		var _input;

		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}

		function _filter(row) {
			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}

		return {
			init: function() {
				var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = _onInputEvent;
				});
			}
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			LightTableFilter.init();
		}
	});

})(document);
/* #END Seaching Reports */

/* #BEGIN Dowload Sheet */
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
/* #END Dowload Sheet */

/* #BEGIN For Show / Hide Parent Ledger */
$(document).ready(function() {
	$("#childLedger").click(function() {
		$('#parentLedgField').show();
		$('.ledgerGroup').focus();
	});
	
	$("#parentLedger").click(function() {
		$('#parentLedgField').hide();
	});
});
/* #END For Show / Hide Parent Ledger */

/* #BEGIN For Show / Hide Parent Category */
$(document).ready(function() {
	$("#childCategory").click(function() {
		$('#parentCatgField').show();
		$('.parentCategoryName').focus();
	});
	
	$("#parentCategory").click(function() {
		$('#parentCatgField').hide();
	});
});
/* #END For Show / Hide Parent Category */


function add_row()
{
	var variantName = $("#variantName").val();
	var purPrice = $("#purPrice").val();
	var mrp = $("#mrp").val();
	var btobPrice = $("#btobPrice").val();
	var btocPrice = $("#btocPrice").val();
	var stock = $("#stock").val();
	
	var table = document.getElementById("data_table");
	var table_len = (table.rows.length)-1;
	//if(!variantName.val())
	//{
	var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='variantName"+table_len+"'>"+variantName+"</td><td id='purPrice"+table_len+"'>"+purPrice+"</td><td id='mrp"+table_len+"'>"+mrp+"</td><td id='btobPrice"+table_len+"'>"+btobPrice+"</td><td id='btocPrice"+table_len+"'>"+btocPrice+"</td><td id='stock"+table_len+"'>"+stock+"</td><td><input type='button' id='edit_button"+table_len+"' value='Edit' class='edit' onclick='edit_row("+table_len+")'> <input type='button' id='save_button"+table_len+"' value='Save' class='save' onclick='save_row("+table_len+")'> <input type='button' value='Delete' class='delete' onclick='delete_row("+table_len+")'><td></tr>";	

	/*var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='name_row"+table_len+"'>"+new_name+"</td><td id='country_row"+table_len+"'>"+new_country+"</td><td id='age_row"+table_len+"'>"+new_age+"</td><td><input type='button' id='edit_button"+table_len+"' value='Edit' class='edit' onclick='edit_row("+table_len+")'> <input type='button' id='save_button"+table_len+"' value='Save' class='save' onclick='save_row("+table_len+")'> <input type='button' value='Delete' class='delete' onclick='delete_row("+table_len+")'></td></tr>";*/
	//}
	alert(table_len);
	var variantName = $("#variantName").val("");
	var purPrice = $("#purPrice").val("");
	var mrp = $("#mrp").val("");
	var btobPrice = $("#btobPrice").val("");
	var btocPrice = $("#btocPrice").val("");
	var stock = $("#stock").val("");
	
}

function delete_row(no)
{
 document.getElementById("row"+no+"").outerHTML="";
}

function edit_row(id)
{
 var variantName = document.getElementById("variantName"+id).innerHTML;
 var purPrice = document.getElementById("purPrice"+id).innerHTML;
 var mrp = document.getElementById("mrp"+id).innerHTML;
 var btobPrice = document.getElementById("btobPrice"+id).innerHTML;
 var btocPrice = document.getElementById("btocPrice"+id).innerHTML;
 var stock = document.getElementById("stock"+id).innerHTML; 

 document.getElementById("name_val"+id).innerHTML="<input type='text' id='name_text"+id+"' value='"+name+"'>";
 document.getElementById("age_val"+id).innerHTML="<input type='text' id='age_text"+id+"' value='"+age+"'>";
	
 document.getElementById("edit_button"+id).style.display="none";
 document.getElementById("save_button"+id).style.display="block";
}

function save_row(no)
{
 var variantName = document.getElementById("variantName"+no).value;
 var purPrice = document.getElementById("purPrice"+no).value;
 var mrp = document.getElementById("mrp"+no).value;
 var btobPrice = document.getElementById("btobPrice"+no).value;
 var btocPrice = document.getElementById("btocPrice"+no).value;
 var stock = document.getElementById("stock"+no).value;

 document.getElementById("variantName"+no).innerHTML = variantName;
 document.getElementById("purPrice"+no).innerHTML = purPrice;
 document.getElementById("mrp"+no).innerHTML = mrp;
 document.getElementById("btobPrice"+no).innerHTML = btobPrice;
 document.getElementById("btocPrice"+no).innerHTML = btocPrice;
 document.getElementById("stock"+no).innerHTML = stock;

 document.getElementById("edit_button"+no).style.display = "block";
 document.getElementById("save_button"+no).style.display = "none";
}