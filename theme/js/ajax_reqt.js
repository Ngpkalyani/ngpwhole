$(document).ready(function() {
    $('#formAddNewLedger').on('submit', function(e) {
		var ledgFormData = document.getElementById("formAddNewLedger");	
		var ledgerType = $('input[name="ledgerType"]:checked', ledgFormData).val();
		var ledgerGroup = $('.ledgerGroup');

		if( !ledgerType ) {
			alert("Please Select Ledger Type");
			return false;
		}
		if( ledgerType == 'C' ) {
			if( !ledgerGroup.val()) {				
				alert("Please Select Ledger Group");
				$('.ledgerGroup').focus();
				return false;
			}
			return true;		
		}
		
		$.ajax({
			url: '/models/ledgers/controller/ledger_add_cntrl.php',
			method: 'POST',
			data: ledgFormData.serialize()
        }).success(function(response) {
			
			/*alert("Kayani");
                // Get the cells
                var $button = $('button[data-id="' + response.id + '"]'),
                    $tr     = $button.closest('tr'),
                    $cells  = $tr.find('td');

                // Update the cell data
                $cells
                    .eq(1).html(response.name).end()
                    .eq(2).html(response.email).end()
                    .eq(3).html(response.website).end();

                // Hide the dialog
                $form.parents('.bootbox').modal('hide');

                // You can inform the user that the data is updated successfully
                // by highlighting the row or showing a message box
                bootbox.alert('The user profile is updated');*/
            });
        });
		e.preventDefault();
});