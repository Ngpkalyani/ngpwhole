/* #BEGIN Product Qty Increament & Decreament */
function qty_decr(qtyID)
{
	var result = document.getElementById('qty'+qtyID);
	var qty = result.value;		
	if( qty > 1 ) {
		result.value--;
	}
	return false;
}
function qty_incr(qtyID)
{
	var result = document.getElementById('qty'+qtyID);
	var result_cart = document.getElementById('quantity'+qtyID);
	var qty = result.value;		
	if( qty >= 1 ) {
		result.value++;
		result_cart.value = result.value;
	}
	return false;	
}
/* #END Product Qty Increament & Decreament */

function validNumeric(number, e) {
	try
	{
		if( window.event ) {
            var charCode = window.event.keyCode;
        }
        else if( number ) {
            var charCode = number.which;
        }
        else {
            return true;
        }
        if( charCode > 31 && (charCode < 48 || charCode > 57) && (e.keyCode < 96 || e.keyCode > 105) ) {
            return false;
        }
        return true;
    }
    catch( err )
	{
        alert(err.Description);
    }
}


function addToCart(action, productCode)
{
	var formData = "";
    if (action != "") {
        switch (action) {
        case "add":
            formData = 'action='+action+
						'&productCode='+productCode+
						'&productName='+$("#productName"+productCode).val()+
						'&productPrice='+$("#productPrice"+productCode).val()+
						'&productMRP='+$("#productMRP"+productCode).val()+
						'&productQuantity='+$("#qty"+productCode).val()+
						'&variantName='+$("#variantName"+productCode).val();						
            break;
        case "remove":
            formData = 'action=' + action + '&productCode=' + productCode;
            break;
        case "empty":
            formData = 'action=' + action;
            break;
        }
    }
	var cartUrl = "cart_process.php";
	
	jQuery.ajax({
        url : cartUrl,
        data : formData,
        type : "POST",
        success : function(data) {
            //$("#cart-item").html(data.items);
			//alert("Item added to Cart!"+data.items);
            /*if (action == "add") {
                $("#add_" + product_code + " img").attr("src",
                        "images/icon-check.png");
                $("#add_" + product_code).attr("onclick", "");
            }*/
        },
        error : function() {
        }
    });
}

$(document).ready(function(){
    $('.modal-cart').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-cart-content').load(dataURL,function(){
            $('#modal-cart').modal({show:true});
        });
    });
});


//$("#parentCatName").attr('required', true);
function sendOTP(mobileNo)
{
    var mobileNo = mobileNo.trim();
    if( mobileNo.length == 10 && $.isNumeric(mobileNo) ) {
        $('.field-otp').show();
        //$('#mobileOTP').val('');
		//$.post("ajax_call.php", $("#productActionForm").serialize());
        $.post("login_process.php", {cmdType: "sendOTP", mobileNo: mobileNo}, function (data) {
			//alert(data);
			var data_value = data.trim();
			var userData = data_value.split("-")[0];
			//alert(userData);
            if( userData == "New" )
            {
				//alert(userData);
                $('.login-form-msg').html("Your profile not registered. Proceed to Registration.");
				$('.signUp-form').show();
                $('#loginType').val('signUp');
				$('#cmdType').val('signUp');
				$("#signUp_name").attr('required', true);
				$("#signUp_email").attr('required', true);
				$("#signUp_city").attr('required', true);
				$("#signIn_signUp_btn").val("Register new Profile");				
            } 
			else
			{
				//alert(userData);
                $('.login-form-msg').html("Your are already registered, please proceed login with OTP.");
				$('#loginType').val('signIn');
				$('#cmdType').val('signIn');
                $('.signUp-form').hide();
                $("#signUp_name").val("");
				$("#signUp_email").val("");
				$("#signUp_city").val("");
				$("#signIn_signUp_btn").val("Confirm Login With OTP");
            }
        });
    } else {
        $('.mobileNoError-msg').html("Enter Correct Mobile number.");
    }
}

function logIn_signUp()
{
	var signIn_mobileNo = $("#signIn_mobileNo").val();
	var signIn_otp = $("#signIn_otp").val();
	var signUp_name = $("#signUp_name").val();
	var signUp_email = $("#signUp_email").val();
	var signUp_city = $("#signUp_city").val();
	var loginType = $("#loginType").val();
	var cmdType = $("#cmdType").val();
	
	if( loginType == "signUp" && cmdType == "signUp" )
	{
		if( !isNaN(signIn_mobileNo) && !isNaN(signIn_otp) && signUp_name != "" && signUp_email != "" && signUp_city != "" ) {
			var inputData = { mobileNo: signIn_mobileNo, 
								otp: signIn_otp,
								name: signUp_name,
								email: signUp_email,
								city: signUp_city,
								loginType: loginType,
								cmdType: cmdType
							};
							
			$.post("login_process.php", inputData, function (data) {
				//alert(data);
				var data_value = data.trim();
				console.log(data);
				//console.log(status);
				
				if( data_value == "Incorrect OTP" ) {
					$('.loginForm-msg').html("<br>Incorrect OTP.");					
				}
				if( data_value == "Registration Fail" ) {
					alert("Registration Fail.");					
				}
				if( data_value == "Login Success" ) {
					alert("Successfully Registration & Logged In.");
					window.location.reload();					
				}
					
			}).fail(function () {});			
				
		}
	}
	else if( loginType == "signIn" && cmdType == "signIn" )
	{
		if( !isNaN(signIn_mobileNo) && !isNaN(signIn_otp) ) {
			var inputData = {mobileNo: signIn_mobileNo, 
								otp: signIn_otp, 
								loginType: loginType, 
								cmdType: cmdType
							};
			$.post("login_process.php", inputData, function (data) {
				//alert(data);
				var data_value = data.trim();
				
				if( data_value == "Incorrect OTP" ) {
					$('.loginForm-msg').html("<br>Incorrect OTP.");
				}
				if( data_value == "Login Fail" ) {
					alert("Login Fail.");
				}
				if( data_value == "Login Success" ) {
					alert("Successfully Logged In.");
					window.location.reload();
				}				
			});
		}
	}
	
}

function getShippingCharges()
{
	var pincode = $("#pincode").val();
	var pincodeNo = pincode.trim();
	
	var subTotal = $("#subTotal").val();
	var finalTotal = 0.00;
	
    if( pincodeNo.length == 6 && $.isNumeric(pincodeNo) ) {
		$('.pincodeErr-msg').html("");
		$.post("dataGet.php", { pincode: pincodeNo, subTotal: subTotal }, function (data) {
			//alert(data);
			var data_value = data.trim();
			//alert(data_value);
			if( data_value > 0 ) {
				$("#shippingCharges").html(data_value);
				$("#shippingChargesValue").val(data_value);
				
				finalTotal = parseFloat(subTotal) + parseFloat(data_value);
				$("#finalTotal").html(parseFloat(finalTotal).toFixed(2)); 
			}
			else {
				$("#shippingCharges").html("0.00");
				$("#finalTotal").html(parseFloat(subTotal).toFixed(2));
			}
		});
	}
	else {
		$('.pincodeErr-msg').html("Invalid Pincode");
	}
		
}



/*function increment_quantity(cart_id, price) {
    var inputQuantityElement = $("#input-quantity-"+cart_id);
    var newQuantity = parseInt($(inputQuantityElement).val()) + 1;
	$(inputQuantityElement).val(newQuantity);
	
    //var newPrice = newQuantity * price;
   // save_to_db(cart_id, newQuantity, newPrice);
}

function decrement_quantity(cart_id, price) {
    var inputQuantityElement = $("#input-quantity-"+cart_id);
    if($(inputQuantityElement).val() > 1) 
    {
    var newQuantity = parseInt($(inputQuantityElement).val()) - 1;
	$(inputQuantityElement).val(newQuantity);
    //var newPrice = newQuantity * price;
    //save_to_db(cart_id, newQuantity, newPrice);
    }
}*/

// JavaScript Document

/*var result = document.getElementById('qty'); 
var qty = result.value; 
if( !isNaN( qty )) 
result.value++;
return false;

var result = document.getElementById('qty');
var qty = result.value;
if( !isNaN( qty ) &amp;&amp; qty &gt; 0 )
result.value--;
return false;*/


/*function move_prodImage($proimage)
{
	var $cartIcon = $(".my-cart");
	alert($proimage);
	var $image = $('<img width="100px" height="100px" src="' + $proimage.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
	alert($image);
	$addTocartBtn.prepend($image);
	var position = $cartIcon.position();
	$image.animate({
		top: position.top,
		left: position.left
	}, 600 , "linear", function() {
		$image.remove();
	});
	
}*/


// Mini Cart
/*paypal.minicart.render({
	action: '#'
});

if (~window.location.search.indexOf('reset=true')) {
	paypal.minicart.reset();
}*/
