<div class="card card-primary">
<form name="signInForm" id="signInForm" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Sign In</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-12 pl">
					<input type="number" name="signIn_mobileNo" id="signIn_mobileNo" value="<?php echo isset($_POST['signIn_mobileNo']) ? $_POST['signIn_mobileNo'] : ""; ?>" placeholder="Enter Mobile No." maxlength="10" required class="form-control" onkeypress="return validNumeric(this.value, event)" />
                    <div class="mobileNoError-msg" ></div>
				</div>
			</div>
			<div class="form-group col-sm-12 row">
            	<div class="col-sm-12 pr">
                    <span class="btn btn-info bg-addbtn pr" onClick="sendOTP($('#signIn_mobileNo').val())">Sign In with OTP</span>
                </div>
			</div>
			<div class="form-group col-sm-12 row field-otp" style="display:none">
				<div class="col-sm-6 pl">
                	<input type="number" name="signIn_otp" id="signIn_otp" value="<?php echo isset($_POST['signIn_otp']) ? $_POST['signIn_otp'] : ""; ?>" placeholder="Enter OTP" required class="form-control" onkeypress="return validNumeric(this.value, event)" />
					<div class="loginForm-msg" ></div>
                </div>
			</div>
            <div class="signUp-form" style="display:none">
            	<div class="form-group col-sm-12 row">
                	<div class="col-sm-12 pl">
                    	<input type="text" name="signUp_name" id="signUp_name" value="<?php echo isset($_POST['signIn_mobileNo']) ? $_POST['signIn_mobileNo'] : ""; ?>" placeholder="Enter Your Name" class="form-control" />
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                	<div class="col-sm-12 pl">
                    	<input type="email" name="signUp_email" id="signUp_email" value="<?php echo isset($_POST['signUp_email']) ? $_POST['signUp_email'] : ""; ?>" placeholder="Enter Your Email ID" class="form-control" />
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                	<div class="col-sm-12 pl">
                    	<select name="signUp_city" id="signUp_city" class="form-control">
                        	<option value="" default>( Select City )</option>
                            <option value="Nagpur">Nagpur</option>
                        </select>
                    </div>
                </div>            
            </div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
                	<input type="hidden" name="loginType" id="loginType" value="signIn" />
                    <input type="hidden" name="cmdType" id="cmdType" value="" />
					<input type="button" name="signIn_signUp_btn" id="signIn_signUp_btn" value="" onClick="return logIn_signUp();" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
    </div>
</form>
</div>