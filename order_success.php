<?php include('project_header.php'); ?>

<body class="minicart-showing">
<div id="page">
	<?php include('project_topbar.php'); ?>
    
    <section class="main-container bounceInUp animated">
    	<div class="container">
        	<div class="product-grid col-sm-12">
            	<div class="category-products pro-coloumn">
                <?php $orderNo = isset($_POST['orderNo']) ? $_POST['orderNo'] : "";	?>
                	<h2 class="text-center">Thank You for shopping with us.</h2>
                    <h2 class="text-center">Your Order <?php print_r($orderNo); ?>has been placed successfully, with deliver your order soon.</h2>
                </div>
            </div>
        </div>
    </section>
    
    <?php include('project_footer.php'); ?>
    <?php include('project_footer_bottom.php'); ?>
</div>
</body>
</html>