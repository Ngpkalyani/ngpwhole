<?php require_once('includes/qtpl_config.php'); ?>
<?php
if( !empty($_POST["variantName"]) && isset($_POST["variantName"]) ) 
{
	//echo $_POST["variantName"];
	$variant = explode(' ',$_POST["variantName"]);
	$variantName = $variant[0];
	$productID = array_pop($variant);
	
	$cond = "products_id='$productID' AND variant_name='$variantName'";
	$order = "";
	$limit = "1";
	$variantPrices = $Variants->select('variants', '', $cond, $order, $limit);
	foreach( $variantPrices as $variantPrice ) {
		$btoc_price = $variantPrice['btoc_price'];
		$mrp = $variantPrice['mrp'];
		?>
        <span class="item_price col-sm-6"><i class="fa fa-rupee-sign"></i> <?php echo $btoc_price; ?></span>
        <input type="hidden" name="productPrice" id="productPrice<?php echo $productID; ?>" value="<?php echo $btoc_price; ?>" />
        <del class="col-sm-6"><i class="fa fa-rupee-sign"></i> <?php echo $mrp; ?></del>
        <input type="hidden" name="productMRP" id="productMRP<?php echo $productID; ?>" value="<?php echo $mrp; ?>" />
        <?php		
	}
}
?>
<?php
if( !empty($_POST['pincode']) && isset($_POST['pincode']) )
{
	$cond = "pincode='".$_POST['pincode']."' AND status>'0'";
	$order = "";
	$limit = "1";
	$shipCharges_list = $Shipping_charge->select($Shipping_charge->table,'', $cond, $order, $limit);
	
	$deliveryCharges = 0;
	foreach( $shipCharges_list as $shipCharges ) {
		if( $_POST['subTotal'] < $shipCharges['order_amt'] ) {
			$deliveryCharges = $shipCharges['shipping_charges'];						
		}
	}
	echo $deliveryCharges;
	
}
?>