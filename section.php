<section class="main-container bounceInUp animated">
	<!--<div class="container-fluid">-->
    <?php 
	foreach( $section_list as $section ) {
		if( $section['status'] > 0 ) {
			$sectionID = $section['sections_id'];
			$sectionName = $section['section_name'];
			$sectionProductIDs = $section['products_id'];
			
			$sectProdID_list = explode(',',$sectionProductIDs);
			$countSectProdID_list = count($sectProdID_list);
			$randomProductList = array();
			if( $countSectProdID_list <= 6 ) {
				$randomProductList[] = $sectionProductIDs;
			}
			else {
				$rand_prod = array_rand($sectProdID_list, 6);
				for( $i = 0; $i <= 5; $i++ ) {					
					$randomProductList[] = $sectProdID_list[$rand_prod[$i]];
				}				
			}
			//echo $randomProductList;
			//echo $sectProdID_list[$rand_prod[5]];
			//echo $sectProdID_list[$rand_prod[2]];
	?>
    	<div class="row">
        	<div class="product-grid col-sm-12">
            	<div class="category-products pro-coloumn">
                	<div class="row">
                    	<div class="see-all">
                        	<div style="height:25px">
                            	<div class="sect-name"><a href="section_result.php?sectionID=<?php echo $sectionID; ?>"><span><?php echo $sectionName; ?></span></a></div>
                                <div class="sect-btn"><a href="section_result.php?sectionID=<?php echo $sectionID; ?>"><span>See-All</span></a></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row col-sm-12">
                    <?php
					$j = 1;
					foreach( $randomProductList as $randomSectProd => $value ) {
						//echo $value;
						$cond = "products_id='$value' AND product_type!='B2B' AND status>'0'";
						$order = "";
						$limit = "1";
						$sectionProduct_list = $Product->select('products', '', $cond, $order, $limit);
						
						foreach ($sectionProduct_list as $product ) {
							$productID = $product['products_id'];
							$productName = $product['product_name'];
							$img = $product['product_img1'];
							$prod_img = '<img src="theme/imgs/product_imgs/'.$img.'" class="pro-image-front" />';
							$logo_img = '<img src="theme/imgs/common_imgs/nw_logo.png" class="pro-image-front" />';
							$productImage = $imgs != "" ? $prod_img : $logo_img;
							
							/* Get Product Variant List */
							$cond = "products_id='$productID' AND status>'0'";
							$order = "variants_id ASC";	
							$prodVariant_list = $Variants->select('variants', '', $cond, $order);
							
							$firstVariant = 1;
							$variantMRP = "";
							$variantPrice = "";
							foreach( $prodVariant_list as $prodVariant_price ) {
								if( $firstVariant <= 1 ) {
									$variantMRP = $prodVariant_price['mrp'];
									$variantPrice = $prodVariant_price['btoc_price'];
									$firstVariant++;
								}
							}
						?>
                        
                    	<div class="col-md-2 col-sm-6 col-xl-2 product">
                        <form name="addToCartForm" method="POST" action="" enctype="multipart/form-data">
                        	<div class="men-pro-item">
                            	<!-- #BEGIN Product Image -->                                
                            	<div class="men-thumb-item" style="height:200px;">
                                	<a href="product.php"><?php echo $productImage; ?></a>
                                    <div class="men-cart-pro"><div class="inner-men-cart-pro"></div></div>
                                </div>
                                
                                <!-- #END Product Image -->
                                
                                <!-- #BEGIN Product Name -->
                                <div class="item-info-product col-sm-12">
                                	<div class="prod-name">
                                    	<span><a href="product.php"><?php echo $productName; ?></a> 
                                        	  <input type="hidden" name="productName" id="productName<?php echo($productID);?>" value="<?php echo $productName; ?>" /> 
                                        </span>
                                    </div>
                                </div>
                                <!-- #END Product Name -->
                                
                                <!-- #BEGIN Product Price --> 
                                <div class="info-product-price">
                                	<div class="prod-price<?php echo($productID);?> col-sm-12">
                                    	<span class="item_price col-sm-6">
                                        	<i class="fa fa-rupee-sign"></i> <span id="price<?php echo($productID);?>"><?php echo $variantPrice; ?></span>
                                            <input type="hidden" name="productPrice" id="productPrice<?php echo($productID);?>" value="<?php echo $variantPrice; ?>" />
                                        </span>
                                        <span class="col-sm-6">
                                        	<i class="fa fa-rupee-sign"></i><del id="mrp<?php echo($productID);?>"><?php echo $variantMRP; ?></del>
                                            <input type="hidden" name="productMRP" id="productMRP<?php echo($productID);?>" value="<?php echo $variantMRP; ?>" />
                                        </span>
                                    </div>
                                </div>
                                <!-- #END Product Price -->
                                
                                <!-- #BEGIN Product Variant -->
                                <div class="info-product-price row">
                                	<div class="prod-varient col-sm-12">
                                    	<div class="col-sm-6" style="float:left">
                                        	<div class="incr-decr quantity">
                                        		<div class="btn-increment-decrement increase" onclick="return qty_decr(<?php echo($productID); ?>);">-</div>
                                            	<input name="productQuantity" class="input-quantity" id="qty<?php echo($productID); ?>" value="1" size="2">
                        						<div class="btn-increment-decrement decrease" onclick="return qty_incr(<?php echo($productID); ?>);">+</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" style="float:left">
                                        	<select name="variantName" id="variantName<?php echo($productID); ?>" onchange="getPrice(this.value+' '+<?php echo($productID); ?>);" class="col-sm-12 variantName" style="height:25px; padding:0px;">
                                            <?php
											foreach( $prodVariant_list as $prodVariant ) {												
												$variantID = $prodVariant['variants_id'];
												$variantName = $prodVariant['variant_name']." ".$prodVariant['measures_name'];
												?>											
                                            	<option value="<?php echo $variantName." ".$variantID; ?>"><?php echo $variantName; ?></option>
                                            <?php
											}
											?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- #END Product Variant -->
                                
                                <!-- #BEGIN Product Add Button -->
                                <div class="info-product-price">
                                	<div class="prod_add-btn col-sm-12">
                                    	<input type="hidden" name="productCode" value="<?php echo $productID ; ?>" />
                                    	<!--<input type="submit" value="Add To cart" class="button btn-cart" onclick="return addToCart();" />-->
                                        <!--<button id="buttonSave" name="buttonSave" class="btn-cart" onclick="addToCart('add','<?php //echo($productID); ?>')">Add To Cart</button>-->
                                        <input type="button" value="Add To Cart" class="button btn-cart" onclick="addToCart('add','<?php echo($productID); ?>')" />
                                        
                                    </div>
                                </div>
                                <!-- #END Product Add Button --> 
                                                           
                            </div>
                            </form>
                        </div>
                        
                        <?php
						$j++;
						}					
					}
					?>
                    </div>
                    
                </div>
            </div>
        </div>
        <?php
		}
	}
	?>
    <!--</div>-->
</section>


