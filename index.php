<?php include('project_header.php'); ?>

<body>
<div id="page">
	<?php include('project_topbar.php'); ?>
    <?php include('project_slider.php'); ?>
    <?php include('category.php'); ?>
    <?php include('section.php'); ?>
    <?php include('project_footer.php'); ?>
    <?php include('project_footer_bottom.php'); ?>
    
    <div class="modal animate col-md-6 col-sm-12" id="modal-cart">
		<div class="modal-cart-content">
    	</div>
	</div>
    
    <div class="modal animate col-md-4 col-sm-12" id="modal-signIn">
		<?php include('nw_otp.php'); ?>
	</div>

</div>

<script type="text/javascript">
function getPrice(val)
{
	var url = 'dataGet.php';
	var productId = val.split(" ").pop();
	
	$.ajax({
		type:'POST',
		url:url,
		data:'variantName='+val,
		success: function(data) {
			//alert(data);
			$(".prod-price"+productId).html(data);
			$("#productPrice"+productId).val((data).find('#productPrice'+productId).val());
			$("#productMRP"+productId).val((data).find('#productMRP'+productId).val());
			//$("#iCustomerName").html($(data).find('#cname').val());
		}		
	});	
}

</script>
</body>
</html>
