<?php require_once('includes/qtpl_config.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="#" type="image/x-icon">
	<title>Nagpurwholesale.com</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- Font Awesome -->
    <link rel="stylesheet" href="theme/css/bootstrap.min.css">
  	<link rel="stylesheet" href="theme/fonts/fontawesome-all.min.css">    
  	<link rel="stylesheet" href="theme/css/custom.min.css">
  	<link rel="stylesheet" href="theme/css/theme_style.css">
    <link rel="stylesheet" href="theme/css/theme_responsive.css">    
    <link rel="stylesheet" href="theme/css/owl.theme.css">
    <link rel="stylesheet" href="theme/css/owl.carousel.css">
    <link rel="stylesheet" href="theme/css/revslider.css">
    <link rel="stylesheet" href="theme/css/flexslider.css">
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
</head>
