<?php require_once('includes/qtpl_config.php'); ?>

<?php
if( !empty($_POST["cmdType"]) && !empty($_POST["mobileNo"]) )
{	
	/* #BEGIN Mobile No. is Existed or Not */
	$existedClient = $Client->check_exist_client($_POST["mobileNo"]);
	
	$status = "New";
	if( $existedClient ) {
		$status = "Existed";
	}
	/* #END Mobile No. is Existed or Not */
	
	/* #BEGIN OTP Generated & Send on above Mobile No. */
	if( !empty($status) && $_POST["cmdType"] == "sendOTP" )
	{
		/* Defined Variables */
		$authkey = '181240AKMuoHen59f58bba';
		$rndNo = rand(100000, 999999);
		$otpNo = intval($rndNo);
		$message = urlencode("Your OTP: ".$otpNo." for www.nagpurwholesale.com");
		$senderID = 'NWSHOP';
		$mobileNo = $_POST["mobileNo"];
		//$mobileNo = intval($mobileNo);
		$url = 'http://control.msg91.com/api/sendotp.php?otp_length=6&authkey=$authkey&message=$massege&sender=$senderID&mobile=$mobileNo&otp=$otpNo';
		$otp = $rndNo;
		$postData  = array(
			'authkey' => $authkey, 
			'mobile' => $mobileNo, 
			'message' => $message, 
			'sender' => $senderID, 
			'otp' => $otpNo,
		);
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 6,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
		));
		
		$response = curl_exec($curl);
		
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if( $err ) {			
			echo "cURL Error #:" . $err;
		}else {
			
			//echo $response;
		}
		$_SESSION["otp"] = $otpNo;
		
		echo $status;
	}
	/* #END OTP Generated & Send on above Mobile No. */
	
	/* #BEGIN Sign Up & Login */
	if( $status = "New" && $_POST["cmdType"] == "signUp" )
	{
		$newOTP = $_SESSION["otp"];
		
		/* #BEGIN Registration */
		if( $_POST["otp"] == $newOTP && $_POST["loginType"] == "signUp" ) {
			/* Define Variables */
			$clientFrmValues['client_name'] = $_POST["name"];
			$clientFrmValues['contact_no'] = $_POST["mobileNo"];
			$clientFrmValues['email_id'] = $_POST["email"];
			$clientFrmValues['city'] = $_POST["city"];
			
			date_default_timezone_set('Asia/Calcutta');
			$clientFrmValues['created_date'] = date('Y-m-d');
			
			$add_client = $Client->save($Client->table, $clientFrmValues);
			
			/* #BEGIN Login */
			if( $add_client ) {
				$newClient = $Client->check_exist_client($_POST["mobileNo"]);
				
				if( $newClient ) {
					$cond = "contact_no='".$_POST["mobileNo"]."' AND status>'0'";
					$order = "";
					$limit = "1";
					$client_login = $Client->select($Client->table, '', $cond, $order, $limit);
					
					if( $client_login ) {
						$_SESSION[$sessName] = $client_login;
						$login_clientID = $_SESSION[$sessName][0]["clients_id"];
						if( isset($login_clientID) ) {
							unset($_POST["mobileNo"]);
							$status = "Login Success";
							echo $status;
						}
					}
					else {
						$status = "Login Fail";
						echo $status;
					}					
				}				
			}
			else {
				$status = "Registration Fail";
				echo $status;
			}
			/* #END Login */
		}
		else {
			$status = "Incorrect OTP";
			echo $status;
		}
		/* #END Registration */
	}
	/* #END Sign Up & Login */
	
	/* #BEGIN Sign Sign In & Login */
	if( $status = "Existed" && $_POST["cmdType"] == "signIn" )
	{
		$newOTP = $_SESSION["otp"];
		
		/* #BEGIN Check OTP */
		if( $_POST["otp"] == $newOTP && $_POST["loginType"] == "signIn" ) {
			$oldClient = $Client->check_exist_client($_POST["mobileNo"]);
			
			/* #BEGIN Login */
			if( $oldClient ) {				
				$cond = "contact_no='".$_POST["mobileNo"]."' AND status>'0'";
				$order = "";
				$limit = "1";
				$client_login = $Client->select($Client->table, '', $cond, $order, $limit);
				
				$_SESSION[$sessName] = $client_login;
				$login_clientID = $_SESSION[$sessName][0]["clients_id"];
				if( isset($login_clientID) ) {
					unset($_POST["mobileNo"]);
					$status = "Login Success";
					echo $status;
				}
				else {
					$status = "Login Fail";
					echo $status;
				}
			}
			else {
				$status = "Registration Fail";
				echo $status;
			}
			/* #END Login */
		}
		else {
			$status = "Incorrect OTP";
			echo $status;
		}
		/* #END Check OTP*/
	}
	/* #END Sign Sign In & Login */	
	
}

?>