<?php require_once('includes/qtpl_config.php'); ?>

<?php
if( !empty($_POST["action"]) ) {
	
	switch($_POST["action"]) {
		case "add":
			if( isset($_COOKIE["shopping_cart"]) )
			{
				$cookie_data = stripslashes($_COOKIE['shopping_cart']);
				$cart_data = json_decode($cookie_data, true);
			}
			else 
			{
				$cart_data = array();
			}
			
			//$item_id_list = array_column($cart_data, 'item_id');
			
			if( in_array($_POST["productCode"], $cart_data) )
			{
				foreach( $cart_data as $keys => $values ) {
					if( $cart_data[$keys]["item_id"] != $_POST["productCode"] ) {
						$cart_data[$keys]["item_quantity"] = $cart_data[$keys]["item_quantity"] + $_POST["productQuantity"];
					}
				}
			}
			else
			{
				$item_array = array(
					'item_id' => $_POST["productCode"],
					'item_name' => $_POST["productName"],
					'item_price' => $_POST["productPrice"],
					'item_MRP' => $_POST["productMRP"],
					'item_quantity' => $_POST["productQuantity"],
					'item_variantName' => $_POST["variantName"]
				);
				$cart_data[] = $item_array;
			}
			
			//$total_items = count($cart_data); //count total items	
			//die(json_encode(array('items'=>$total_items))); //output json	
			
			$item_data = json_encode($cart_data);			
			setcookie('shopping_cart', $item_data, time() + (86400 * 30));
			//echo $item_data;
			break;
			
			
			case "remove":
				$cookie_data = stripslashes($_COOKIE['shopping_cart']);
				$cart_data = json_decode($cookie_data, true);
				foreach($cart_data as $keys => $values) {
					if( $cart_data[$keys]['item_id'] == $_POST["productCode"] ) {
						unset($cart_data[$keys]);
						$item_data = json_encode($cart_data);
						setcookie("shopping_cart", $item_data, time() + (86400 * 30));
					}
				}
			break;
			
	}
}
?>

<div class="card card-primary">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Shopping Cart</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
        <div class="modal-content">
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 50vh); overflow-y:auto;">
        <?php
		if( isset($_COOKIE["shopping_cart"]) )
		{
			$total = 0;
			$cookie_data = stripslashes($_COOKIE['shopping_cart']);
			$cart_data = json_decode($cookie_data, true);
			?>
            <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped order-table">
            	<thead>
					<tr>
						<th>SN</th>
						<th>Product Name</th>
						<th>Variant</th>
						<th>QTY</th>
                        <th>Price</th>
                        <th>AMT</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
				$sr_no = 1;
				foreach( $cart_data as $keys => $values ) {
					?>
                	<tr>
                    	<td><?php echo($sr_no); ?></td>
                        <td><?php echo $values["item_name"]; ?></td>
                        <td><?php echo $values["item_variantName"]; ?></td>
                        <td><?php echo $values["item_quantity"]; ?></td>
                        <td><?php echo $values["item_price"]; ?></td>
                        <td><?php echo number_format($values["item_quantity"] * $values["item_price"], 2);?></td>
                        <td><a onclick="addToCart('remove','<?php echo $values["item_id"]; ?>')"><span class="btn-delete"><i class="fa fa-trash-alt"></i></span></a></td>
                    </tr>
                    <?php
					$total = $total + ($values["item_quantity"] * $values["item_price"]);
					$sr_no++;
				}
				?>
                </tbody>
                <tfoot>
                 	<tr>
                    	<th colspan="3"></th>
                        <th><?php echo($sr_no-1); ?></th>
                        <th>Total</th>
                        <th><?php echo number_format($total, 2); ?></th>
                        <th>&nbsp;</th>
                    </tr>
                 </tfoot>
             </table>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<!--<div class="card-footer">-->
				<div class="form-group">
                <?php
				$login_clientID = "";
				if( isset($_SESSION[$sessName]) ) {
					$login_clientID = $_SESSION[$sessName][0]['clients_id'];
				}
				if( !empty($login_clientID) ) {
					?>
                	<a href="nw_cart.php"><span class="btn btn-info bg-addbtn pr">Checkout</span></a>
                    <?php
				}
				else {
					?>
                    <span class="btn btn-info bg-addbtn pr" data-toggle="modal" data-target="#modal-signIn">Checkout</span>
                    <?php
				}
				?>
				</div>
			<!--</div>-->
		<!--</div>-->
        <div>
        <!-- #End Form Footer -->
        <?php
		}
		else
		{
		?>
        <div class="form-group col-sm-12 row">
        	<div class="col-sm-12 pl">Your Cart is Empty.</div>
        </div>
        <?php
		}
		?>
        
</div>
</form>
</div>




