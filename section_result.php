<?php include('project_header.php'); ?>
  
<body>
<div id="page">
	<?php include('project_topbar.php'); ?>
    <?php include('project_slider.php'); ?>
    <?php include('category.php'); ?>
<section class="main-container bounceInUp animated">
	<!--<div class="container-fluid">-->
    	<div class="row">
        	<div class="product-grid col-sm-12">
            	<div class="category-products pro-coloumn">
                	<div class="row">
                    	<div class="see-all">
                        	<div style="height:25px">
                            	<div class="sect-name"><a href="#"><span>Grocery</span></a></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                    <?php
					for( $i = 1; $i <= 6; $i++ ) {
						?>
                    	<div class="col-md-2 col-sm-6 col-xl-2 product">
                        	<div class="men-pro-item">
                            	<!-- #BEGIN Product Image -->
                            	<div class="men-thumb-item">
                                	<a href="product.php"><img src="theme/imgs/common_imgs/nw_logo.png" alt="" class="pro-image-front" /></a>
                                </div>
                                <!-- #END Product Image -->
                                
                                <!-- #BEGIN Product Name -->
                                <div class="item-info-product col-sm-12">
                                	<div class="prod-name">
                                    	<span><a href="product.php">Apple</a></span>
                                    </div>
                                </div>
                                <!-- #END Product Name -->
                                
                                <!-- #BEGIN Product Price --> 
                                <div class="info-product-price">
                                	<div class="prod-price col-sm-12">
                                    	<span class="item_price col-sm-6">$ 45.99</span>
                                        <del col-sm-6>$ 69.71</del>
                                    </div>
                                </div>
                                <!-- #END Product Price -->
                                 
                                <!-- #BEGIN Product Variant -->
                                <div class="info-product-price row">
                                	<div class="prod-varient col-sm-12">
                                    	<div class="col-sm-6" style="float:left">
                                        	<div class="incr-decr quantity">
                                        		<div class="btn-increment-decrement" onClick="decrement_quantity(<?php echo($i); ?>,'<?php //echo($rate); ?>')">-</div>
                                            	<input class="input-quantity" id="input-quantity-<?php echo($i); ?>" value="1<?php //echo $item["quantity"]; ?>" size="2">
                        						<div class="btn-increment-decrement" onClick="increment_quantity(<?php echo($i); ?>,'<?php //echo($rate); ?>')">+</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" style="float:left">
                                        	<select name="variat" style="width:100%; height:25px; padding:0px;">
                                            	<option value="">250 Gm</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- #END Product Variant -->
                                
                                <!-- #BEGIN Product Add Button -->
                                <div class="info-product-price">
                                	<div class="prod_add-btn col-sm-12">
                                    Add To Cart
                                    </div>
                                </div>
                                <!-- #END Product Add Button -->                            
                            </div>
                        </div>
                        <?php
					}
					?>
                    </div>
                    
                </div>
            </div>
        </div>
    <!--</div>-->
</section>


    <?php include('project_footer.php'); ?>
    <?php include('project_footer_bottom.php'); ?>

</div>
</body>
</html>