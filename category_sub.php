<?php include('project_header.php'); ?>
<?php
$mainCatgIDName = isset($_GET['mainCatgIDName']) ? $_GET['mainCatgIDName'] : "";
$mainCatgIDName = explode(',', $mainCatgIDName);
$mainCatgID = $mainCatgIDName[0];
$mainCatgName = $mainCatgIDName[1];

$cond = "parent_id='$mainCatgID' AND category_type='C' AND status>0";
$order = "sort_order ASC";
$subCategory_list = $Category->select('categories', '', $cond, $order);
$mainAsSub_category = "";
foreach( $subCategory_list as $subCategory ) {
	$mainAsSub_category = $subCategory['category_name'] == $mainCatgName ? "1" : "0";
}
?> 
  
<body>
<div id="page">
	<?php include('project_topbar.php'); ?>
    <?php include('project_slider.php'); ?>
    <?php 
	if( $mainAsSub_category == 1 ) {
		//include('pages/sub_category.php');
	}
	else {
		include('pages/sub_category.php');
	}
	?>
	<?php include('section.php'); ?>
    <?php include('project_footer.php'); ?>
    <?php include('project_footer_bottom.php'); ?>

</div>
</body>
</html>