<header>
	<!-- #BEGIN Logo, MenuBar & Cart	-->
	<div id="header">
    	<div class="header-container container">
        	<div class="row">
            	<!-- #BEGIN Logo -->
                <div class="logo">
                	<a href="<?php echo($baseUrl); ?>" title="Nagpur Wholesale"><div><img src="theme/imgs/common_imgs/nw_logo.png" alt="Nagpur Wholesale" height="75px"></div></a>
                </div>
                <!-- #END Logo -->
                
                <!-- #BEGIN Menu Bar -->
                <div class="fl-nav-menu col-sm-10">
                <!--<input type="search" name="search" placeholder="Search Anything" class="form-control col-sm-6"  />-->
                	<!--<nav>
                    	<div class="nav-inner">
                        	<ul id="nav" class="hidden-xs">
                            	<li></li>
                            </ul>
                        </div>
                    </nav>-->
                </div>
                <!-- #END Menu Bar -->
                
                <!-- #BEGIN Cart -->
                <div class="fl-header-right">
                	<div class="fl-cart-contain" id="cartBar">
                    	<div class="mini-cart my-cart">
                        	<div class="basket"> <a data-href="cart_process.php" class="modal-cart"><span></span></a></div>
                            <div class="fl-mini-cart-content">
                            	<div class="block-subtitle">
                                	<div class="top-subtotal"><i class="fa fa-shopping-cart"></i><span class="price">$259.99</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END Cart -->                
                
            </div>
        </div>
    </div>
    <!-- #END Logo, MenuBar & Cart -->
</header>