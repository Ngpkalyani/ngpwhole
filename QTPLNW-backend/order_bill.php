<?php require_once('../includes/qtpl_backend_config.php'); ?>
<?php //include('modules/views/order/view_order_bill.php'); ?>
<?php
$orderID = isset($_GET['orderID']) ? $_GET['orderID'] : "";
if( !empty($orderID) )
{
	$orderTable = 'orders';
	$orderTempTable = 'order_templates';
	$clientTable = 'clients';
	
	$condOrder = "orders_id='$orderID'";
	$orderOrder = "";
	$limitOrder = "1";
	$orderBills = $Order->select($orderTable, '', $condOrder, $orderOrder, $limitOrder);
	
	$clients_id = "";
	$contact_no = "";
	$order_no = "";
	$order_date_time = "";
	$sub_total = 0.00;
	$shipping_charges = 0;
	$wallet_disc = 0;
	$final_total = 0;
	foreach( $orderBills as $bill ) {
		$clients_id = $bill['clients_id'];
		$contact_no = $bill['contact_no'];
		$order_no = $bill['order_no'];		
		$order_date_time = $bill['order_date_time'];
		$sub_total = $bill['sub_total'];
		$shipping_charges = $bill['shipping_charges'];
		$wallet_disc = $bill['wallet_disc'];
		$final_total = $bill['final_total'];
	}
	
	$condClient = "clients_id='$clients_id' AND contact_no='$contact_no'";
	$orderClient = "";
	$limitClient = "1";
	$clientDetails = $Order->select($clientTable, '', $condClient, $orderClient, $limitClient);
	
	$client_name = "";
	$pincode = "";
	$address = "";
	foreach( $clientDetails as $client ) {
		$client_name = $client['client_name'];
		$pincode = $client['pincode'];
		$address = $client['address'];		
	}
	?>
	
<div class="card card-primary">
	<div class="form-report">
    	<!-- Form Header -->
    	<div class="card-header col-sm-12">
        	<div class="col-sm-8 pl"><label>Bill</label></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
        </div>
        <!-- #End Form Header -->
        <div class="modal-header">
        	<div class="col-sm-12">
            	<div class="col-sm-8 pl">
                	<div><b>Customer Name & Contact: </b><?php echo $client_name; ?> &nbsp; <?php echo $contact_no; ?></div>
                    <div><b>Address : </b> <?php echo $address; ?></div>
                    <div><b>Pincode : </b> <?php echo $pincode; ?></div>
                </div>
                <div class="col-sm-4 pl">
                	<div><b>Invoice No :</b> <?php echo $order_no; ?></div>
                   	<div><b>Invoice Date : </b> <?php echo $order_date_time; ?></div>                                        
                </div>
            </div>
        </div>
        <!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 80px); overflow-y:auto;">
        	<div class="table-responsive">
        	<table class="table table-bordered table-hover table-striped">
           		<thead>
                	<tr>
                    	<th>SN</th>
                        <th>Product Name</th>
                        <th>Variant</th>
                        <th>QTY</th>
                        <th>RATE</th>
                        <th>AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                <?php
				$condOdrProd = "orders_id='$orderID'";
				$orderOdrProd = "";
				$orderProducts = $Order->select($orderTempTable, '', $condOdrProd, $orderOdrProd);
				$sr_no = 1;
				foreach( $orderProducts as $product ){
				?>
                	<tr>
                    	<td><?php echo($sr_no++); ?></td>
                        <td><?php echo $product['product_name']; ?></td>
                        <td><?php echo $product['variant']; ?></td>
                        <td><?php echo $product['quantity']; ?></td>
                        <td><?php echo $product['rate']; ?></td>
                        <td><?php echo $product['amount']; ?></td>
                    </tr>
                    <?php
				}
				?>
                </tbody>
                <tfoot>
                	<tr>
                    	<th><?php echo($sr_no - 1); ?></th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th colspan="2">Sub Total</th>
                        <th><?php echo $sub_total; ?></th>
                    </tr>
                    <?php
					if( $shipping_charges != 0 ) {
						?>
                    <tr>
                    	<th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th colspan="2">Shipping Charges (+)</th>
                        <th><?php echo $shipping_charges; ?></th>
                    </tr>
                    <?php
					}
					?>
                    <?php
					if( $wallet_disc != 0 ) {
						?>
                    <tr>
                    	<th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th colspan="2">Wallet Discount (-)</th>
                        <th><?php echo $wallet_disc; ?></th>
                    </tr>
                    <?php
					}
					?>                    <tr>
                    	<th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th colspan="2">Total</th>
                        <th><?php echo $final_total; ?></th>
                    </tr>
                </tfoot>
            </table>
            </div>
        </div>
        <!-- #End Form Body -->
    </div>
</div>    
    <?php
}
?>