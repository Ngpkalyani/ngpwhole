  <!-- Main Sidebar Container -->

  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../theme/imgs/common_imgs/nw_logo.png" alt="Nagpurwholesale.com" class="brand-image elevation-3">
      <span class="brand-text font-weight-light">Nagpurwholesale.com</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="index.php" class="nav-link active">
              <i class="nav-icon fa fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="user.php" class="nav-link">
              <i class="nav-icon fa fa-user-friends"></i>
              <p>Employee</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="client.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Clients</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="order.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Orders</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="banner.php" class="nav-link">
              <i class="nav-icon fa fa-user-friends"></i>
              <p>Banners</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="section.php" class="nav-link">
              <i class="nav-icon fa fa-coins"></i>
              <p>Sections</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="category.php" class="nav-link">
              <i class="nav-icon fa fa-coins"></i>
              <p>Category</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="brand.php" class="nav-link">
              <i class="nav-icon fa fa-coins"></i>
              <p>Brand</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="product.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Products</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Purchase</p>
            </a>
          </li>          
          <li class="nav-item">
            <a href="vendor.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Vendor</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="ledgers.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Ledgers</p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="delivery_slot.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Delivery Slots</p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="shipping_charge.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Shipping Charges</p>
            </a>
          </li>        
          <li class="nav-item">
            <a href="login1.php" class="nav-link">
              <i class="nav-icon fa fa-gift"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
