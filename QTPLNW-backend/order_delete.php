<?php require_once('../includes/qtpl_backend_config.php'); ?>

<?php
$orderID = isset($_GET['orderID']) ? $_GET['orderID'] : "";
if( isset($_POST['deleteOrderBill']) )
{
	$frmValues['status'] = "0";
	$cond = "orders_id='$orderID'";
	$delete_order = parent::save('orders', $frmValues, $cond);
}
echo $orderID;
?>
<div class="card card-primary">
<form name="formDeleteOrder" id="formDeleteOrder" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Delete Bill</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-12 pl"><label class="control-label">Are you sure you want to delete ?</label></div>
			</div>
            
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="deleteOrderBill" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>
