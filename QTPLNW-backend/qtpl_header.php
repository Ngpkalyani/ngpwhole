<?php require_once('../includes/qtpl_backend_config.php'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nagpur Wholesale | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../theme/fonts/fontawesome-all.min.css">
  <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/css/custom.min.css">
  <link rel="stylesheet" href="../theme/css/style.css">
  <link rel="stylesheet" href="../theme/css/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="../theme/css/datepicker.css">
  <link rel="stylesheet" href="../theme/css/bootstrap-tagsinput.css"> 
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
