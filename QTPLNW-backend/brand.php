<?php include('qtpl_header.php'); ?>
<?php include('qtpl_topbar.php'); ?>
<?php include('qtpl_sidebar.php'); ?>
<div class="content-wrapper" style="min-height: 540px;">
	<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
         
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-md-12">
    			<?php include('modules/views/brand/view_brand_list.php'); ?>
    		</div>
    		</div>
    	</div>
    </section>
</div>
<?php include('qtpl_footer.php'); ?>
<script>
$(document).ready(function(){
    $('.modal-editBrand').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-editBrand-content').load(dataURL,function(){
            $('#modal-editBrand').modal({show:true});
        });
    });
});
</script>