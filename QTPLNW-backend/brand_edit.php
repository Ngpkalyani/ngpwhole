<?php require_once('includes/config.php'); ?>
<?php
$brandID = isset($_GET['brandID']) ? $_GET['brandID'] : "";
if( !empty($brandID) )
{
	$brand_name = "";
	$imgs = "";
	foreach( $brand_list as $brand ) {
		if( $brand['brands_id'] == $brandID ) {
			$brand_name = $brand['brand_name'];
			$imgs = $brand['imgs'];
		}
	}
	
	$brandImages = '<img src="theme/imgs/brand_imgs/'.$imgs.'" width="40px" height="40px" class="img-circle" />';
?>
<div class="card card-primary">
<form name="formUpdateBrand" id="formUpdateBrand" method="post" action="" enctype="multipart/form-data">
	<input type="hidden" name="brandID" value="<?php echo($brandID); ?>" />
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Update Brand</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label class="control-label">Brand Name *</label></div>
				<div class="col-sm-8 pl">
					<input type="text" name="brandName" value="<?php echo isset($brand_name) ? $brand_name : ""; ?>" required placeholder="Brand Name" class="form-control" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Brand Image *</label></div>
				<div class="col-sm-8 pr">
					<input type="file" name="brandImage" value="<?php echo $imgs; ?>" /><?php echo $brandImages; ?>
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="updateBrand" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>
<?php
}
?>
