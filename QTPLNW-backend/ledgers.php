<?php include('qtpl_header.php'); ?>
<?php include('qtpl_topbar.php'); ?>
<?php include('qtpl_sidebar.php'); ?>
<div class="content-wrapper" style="min-height: 540px;">
	<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
         
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-md-12">
    			<?php include('modules/views/ledgers/view_ledgers_list.php'); ?>
    		</div>
    		</div>
    	</div>
    </section>
</div>
<?php include('qtpl_footer.php'); ?>