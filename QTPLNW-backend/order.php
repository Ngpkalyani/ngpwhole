<?php include('qtpl_header.php'); ?>
<?php include('qtpl_topbar.php'); ?>
<?php include('qtpl_sidebar.php'); ?>
<div class="content-wrapper" style="min-height: 540px;">
	<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
         
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-md-12">
    			<?php include('modules/views/order/view_order_list.php'); ?>
    		</div>
    		</div>
    	</div>
    </section>
</div>
<?php include('qtpl_footer.php'); ?>

<script type="text/javascript">
$(document).ready(function() {
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy HH:ii P",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
});

$(document).ready(function(){
    $('.modal-viewOrder').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-viewOrder-content').load(dataURL,function(){
            $('#modal-viewOrder').modal({show:true});
        });
    });
});

$('#modal-deleteOrder').on('show.bs.modal', function (event) {
	
          var button = $(event.relatedTarget) // Button that triggered the modal
          var recipient = button.data('id') // Extract info from data-* attributes
          var modal = $(this);
          var dataString = 'orderID=' + recipient;
		  
            $.ajax({
                type: "GET",
                url: "order_delete.php",
                data: dataString,
                cache: false,
                success: function (data) {
                    console.log(data);
                    modal.find('.modal-deleteOrder-content').html(data);
                },
                error: function(err) {
                    console.log(err);
                }
            });  
    })

// When the document is ready
$(document).ready(function () {
	$('#startDate').datepicker({
		format: "dd-mm-yyyy"
		});
});

$(document).ready(function () {
	$('#endDate').datepicker({
		format: "dd-mm-yyyy"
	});
});

</script>