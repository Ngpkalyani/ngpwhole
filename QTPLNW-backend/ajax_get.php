<?php require_once('../includes/qtpl_backend_config.php'); ?>
<?php 
if( !empty($_POST["mainCategoryName"]) && isset($_POST["mainCategoryName"]) ) 
{
	?>
	<option value="<?php echo isset($_POST['subCategoryName']) ? $_POST['subCategoryName'] : ""; ?>" selected> --- Sub Category --- <?php echo isset($_POST['subCategoryName']) ? $_POST['subCategoryName'] : ""; ?></option>
    <?php
	foreach( $category_list as $category ) {
		$categoryID = $_POST["mainCategoryName"];
		$category_name = $category['category_name'];
		if( $category['category_type'] == 'C' && $category['parent_id'] == $categoryID && $category['status'] > 0 ) {
			?>
            <option value="<?php echo $category_name; ?>"><?php echo $category_name; ?></option>
            <?php
		}
	}
}
?>