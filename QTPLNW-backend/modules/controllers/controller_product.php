<?php
ini_set('max_execution_time', 300);

$Product = new Product;

if( isset($_POST['addNewProduct']) )
{
	$Product->add_product($_POST);
}

if( isset($_POST['addBulkProduct']) )
{
	$Product->add_bulk_product($_POST);
}
?>