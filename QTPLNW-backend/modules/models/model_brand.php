<?php
class Brand extends QTPLDBConfig
{
	var $table = "brands";
	public $data;
	
	public function check_exist_brand($brandName)
	{
		try
		{
			$sql = "SELECT * FROM $this->table WHERE brand_name='$brandName'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();			
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}		
	}
	
	public function add_brand($formValues)
	{
		try
		{
			$formValues['brand_name'] = isset($formValues['brandName']) ? $formValues['brandName'] : "";
			
			$check_exist = self::check_exist_brand($formValues['brandName']);
			
			if($check_exist['status'] == 'error') {				
				echo '<script>alert("Brand Already Exist");</script>';
				//$data = $check_exist;				
			}
			else {
				/* #Begin For Photo Upload */
				$src = $_FILES['brandImage']['tmp_name'];
				$dest = "../theme/imgs/brand_imgs/".$formValues['brand_name'].".jpg";
				if( move_uploaded_file($src, $dest) ) {
					$formValues['imgs'] = $formValues['brand_name'].".jpg";
				}
				/* #End For Photo Upload */
				$add_brand = parent::save($this->table, $formValues);
				
				if( !$add_brand ) {
					throw new exception($add_brand);
				}
				
				echo '<script>alert("Success");</script>';
				header("location:	brand.php");
			}
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	
	public function update_brand($formValues)
	{
		try
		{				
			$formValues['brand_name'] = isset($formValues['brandName']) ? $formValues['brandName'] : "";
			
			/* #Begin For Photo Upload */
			$src = $_FILES['brandImage']['tmp_name'];
			$dest = "theme/imgs/brand_imgs/".$formValues['brandName'].".jpg";
			if( move_uploaded_file($src, $dest) ) {
				$formValues['imgs'] = $formValues['brandName'].".jpg";
			}
			
			$brandID = isset($formValues['brandID']) ? $formValues['brandID'] : "";
			
			$cond = "brands_id='$brandID'";
			
			$update_brands = parent::save($this->table, $formValues, $cond);
			
			if( !$update_brands ) {
				throw new exception($add_brand);
			}
			
			echo '<script>alert("Success");</script>';
			header("location:	brand.php");
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	
	
}
?>