<?php
class Category extends QTPLDBConfig
{
	var $table = "categories";
	
	public function add_category($formValues)
	{
		try
		{
			$categoryName = isset($formValues['categoryName']) ? $formValues['categoryName'] : "";
			$parentCategoryName = isset($formValues['parentCategoryName']) ? $formValues['parentCategoryName'] : "";
			$categoryType = isset($formValues['categoryType']) ? $formValues['categoryType'] : "";
			$sortOrder = isset($formValues['sortOrder']) ? $formValues['sortOrder'] : "";
			
			switch ($categoryType ) {
				case 'C':
				$category_type = 'C';
				break;
				
				default:
				$category_type = 'P';
				break;
			}
			
			/* #BEGIN Get Parent Category ID */
			$parentID = 0;
			if( $categoryType == 'C' && !empty($parentCategoryName) ) {
				$sql = "SELECT * FROM $this->table WHERE category_name='$parentCategoryName' AND category_type='P'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$parentID = $res_set['categories_id'];
			}
			/* #END Get Parent Category ID */
			
			/* #Begin For Photo Upload */
			$src = $_FILES['categoryImage']['tmp_name'];
			$dest = "../theme/imgs/category_imgs/".$categoryName.".jpg";
			if( move_uploaded_file($src, $dest) ) {
				$formValues['imgs'] = $categoryName.".jpg";
			}
			/* #End For Photo Upload */
			
			$formValues['category_name'] = $categoryName;
			$formValues['users_id'] = '1';
			$formValues['category_type'] = $category_type;
			$formValues['parent_id'] = $parentID;
			$formValues['sort_order'] = $sortOrder;
			
			$add_category = parent::save($this->table, $formValues);
			
			if( !$add_category ) {
				throw new exception($add_category);
			}
			echo '<script>alert("Success");</script>';
			header("location:	category.php");
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>