<?php
class Shipping_charge extends QTPLDBConfig
{
	var $table = "shipping_charges";
	public $data;
	
	public function check_exist_pincode($pincode)
	{
		try
		{
			$sql = "SELECT * FROM $this->table WHERE pincode='$pincode'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();			
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}		
	}
	
	
	public function add_pincode($formValues)
	{
		try
		{
			$pincodeFrmValues['pincode'] = isset($formValues['pincode']) ? $formValues['pincode'] : "";
			$pincodeFrmValues['order_amt'] = isset($formValues['orderAmount']) ? $formValues['orderAmount'] : "";
			$pincodeFrmValues['shipping_charges'] = isset($formValues['shippingCharges']) ? $formValues['shippingCharges'] : "";
			
			$check_exist = self::check_exist_pincode($formValues['pincode']);
			
			if( $check_exist['status'] == 'error' ) {				
				echo '<script>alert("Pincode Already Exist");</script>';
				//$data = $check_exist;				
			}
			else {
				$add_pincode = parent::save($this->table, $pincodeFrmValues);
				
				if( !$add_pincode ) {
					throw new exception($add_pincode);
				}
				
				echo '<script>alert("Success");</script>';
				header("location:	shipping_charge.php");
			}
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>