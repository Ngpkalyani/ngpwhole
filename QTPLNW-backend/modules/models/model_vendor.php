<?php
class Vendor extends QTPLDBConfig
{
	var $table = "vendors";
	
	public function add_vendor($formValues)
	{
		try {
			$sql = "SELECT * FROM vendors ORDER BY vendors_id DESC LIMIT 1";
			$res = parent::run($sql);
			$res_set = mysqli_fetch_assoc($res);
			$lastVendorCode = $res_set['vendor_code'];
			
			//$newVendorCode = "";
			if( empty($lastVendorCode) ) {
				$newVendorCode = "NWV-001";
			}
			else {				
				$lastVendorNo = explode('-', $lastVendorCode);
				$lastVendNo = $lastVendorNo[1];
				$firstDigit = substr($lastVendNo, 0, 1);
				$firstTwoDigit = substr($lastVendNo, 0, 2);
				$lastDigit = substr($lastVendNo,-1);
				$lastTwoDigit = substr($lastVendNo,-2);
				$newVendorNo = $lastVendNo + 001;
				
				if( $firstTwoDigit == '00' && $lastDigit < '9' ) {					
					$newVendorCode = "NWV-00".$newVendorNo;
				}
				else if( $firstTwoDigit == '00' && $lastDigit == '9' ) {
					$newVendorCode = "NWV-0".$newVendorNo;
				}
				else if( $firstTwoDigit != '00' && $firstDigit == '0' ) {
					$newVendorCode = "NWV-".$newVendorNo;					
				}
				else {
					$newVendorCode = "NWV-".$newVendorNo;
				}
			}	
			
			//print_r($newVendorCode);
			$vendorFrmValues['vendor_code'] = $newVendorCode;
			$vendorFrmValues['users_login_id'] = "123";
			$vendorFrmValues['ledgers_id'] = "123";
			$vendorFrmValues['firm_name'] = isset($formValues['firmName']) ? $formValues['firmName'] : "";
			$vendorFrmValues['properietor_name'] = isset($formValues['proprietorName']) ? $formValues['proprietorName'] : "";
			$vendorFrmValues['email_id'] = isset($formValues['emailID']) ? $formValues['emailID'] : "";
			$vendorFrmValues['contact_no'] = isset($formValues['contactNo']) ? $formValues['contactNo'] : "";
			$vendorFrmValues['address'] = isset($formValues['address']) ? $formValues['address'] : "";
			$vendorFrmValues['gst_no'] = isset($formValues['gstNo']) ? $formValues['gstNo'] : "";
			$vendorFrmValues['pan_no'] = isset($formValues['panNo']) ? $formValues['panNo'] : "";
			$vendorFrmValues['bank_name'] = isset($formValues['bankName']) ? $formValues['bankName'] : "";
			$vendorFrmValues['bank_ac_no'] = isset($formValues['bankAcNo']) ? $formValues['bankAcNo'] : "";
			$vendorFrmValues['ifsc_code'] = isset($formValues['ifscCode']) ? $formValues['ifscCode'] : "";
			$vendorFrmValues['users_id'] = '1';
			
			$add_vendor = parent::save($this->table, $vendorFrmValues);
			
			if( $add_vendor ) {
				$firm_name = $vendorFrmValues['firm_name'];
				$properietor_name = $vendorFrmValues['properietor_name'];
				$contact_no = $vendorFrmValues['contact_no'];				
				
				$sql = "SELECT * FROM $this->table WHERE firm_name='$firm_name' AND properietor_name='$properietor_name' AND contact_no='$contact_no'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$vendorsID = $res_set['vendors_id'];
				
				if( !empty($vendorsID) ) {
					/* #BEGIN Create User Login */
					$vendorLoginValues['user_type'] = 'V';
					$vendorLoginValues['users_id'] = $vendorsID;
					$vendorLoginValues['username'] = isset($formValues['username']) ? $formValues['username'] : "";
					$vendorLoginValues['password'] = isset($formValues['password']) ? $formValues['password'] : "";
					$vendorLoginValues['password'] = base64_encode($formValues['password']);
					
					$add_vendor_login = parent::save('users_login', $vendorLoginValues);
					/* #END Create User Login */
					
					/* #BEGIN Create Ledger */
					$ledg_values['users_id'] = '1';
					$ledg_values['ledgers_groups_id'] = '6';
					$ledg_values['ledger_name'] = $vendorFrmValues['firm_name']." A/c";
					$ledg_values['relation_id'] = $vendorsID;
					$ledg_values['relation_name'] = 'Vendor';
				
					$add_ledgers = parent::save('ledgers', $ledg_values);
					/* #END Create Ledger */
				}
				
				if( $add_ledgers ) {
					$sql = "SELECT * FROM users_login WHERE user_type='V' AND users_id='$vendorsID'";
					$res = parent::run($sql);
					$res_set = mysqli_fetch_assoc($res);
					$userLoginID = $res_set['users_login_id'];
					
					$sql = "SELECT * FROM ledgers WHERE relation_id='$vendorsID' AND relation_name='Vendor'";
					$res = parent::run($sql);
					$res_set = mysqli_fetch_assoc($res);
					$ledgersID = $res_set['ledgers_id'];
				
					$cond = "vendors_id='$vendorsID'";
					$field_name['ledgers_id'] = $ledgersID;
					$field_name['users_login_id'] = $userLoginID;
					
					$update_vendors = parent::save($this->table, $field_name, $cond);					
				}
					
			}
			else if( !$add_vendor || !$add_ledgers ) {
				throw new exception($add_ledgers);
			}
			echo '<script>alert("Success");</script>';
			header("location:	vendor.php");			
		}
		catch(Exception $e) {
			echo $e;
		}		
	}
}
?>