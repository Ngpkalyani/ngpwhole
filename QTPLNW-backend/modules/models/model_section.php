<?php
class Section extends QTPLDBConfig
{
	var $table = "sections";
	public $data;
	
	public function check_exist_section($sectionName)
	{
		try
		{
			$sql = "SELECT * FROM $this->table WHERE section_name='$sectionName'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();			
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}		
	}
	
	public function add_section($formValues)
	{
		try
		{
			$formValues['section_name'] = isset($formValues['sectionName']) ? $formValues['sectionName'] : "";
			$formValues['sort_order'] = isset($formValues['sortOrder']) ? $formValues['sortOrder'] : "";
			$product_list = isset($formValues['productListName']) ? $formValues['productListName'] : "";
			
			$product_list = explode(',',$product_list);
			$countProduct = count($product_list);
			//echo $product_list[0];
			$productIDs = array();
			for( $i = 0; $i <= $countProduct; $i++ ) {
				$sql = "SELECT * FROM products WHERE product_name='".$product_list[$i]."'";
				$res = parent::run($sql);
				while( $rows = mysqli_fetch_array($res) ) {
					$productIDs[] = $rows['products_id'];
				}			
			}
			
			$productID = implode(',',$productIDs);			
			$formValues['products_id'] = $productID;
			
			$check_exist = self::check_exist_section($formValues['sectionName']);
			if($check_exist['status'] == 'error') {				
				echo '<script>alert("Section Already Exist");</script>';
				//$data = $check_exist;				
			}
			else {
				$add_section = parent::save($this->table, $formValues);
				
				if( !$add_section ) {
					throw new exception($add_section);
				}
				
				echo '<script>alert("Success");</script>';
				header("location:	section.php");
			}
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>