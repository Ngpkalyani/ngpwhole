<?php
class Client extends QTPLDBConfig
{
	var $table = "clients";	
	public $data;
	
	public function check_exist_client($contactNo)
	{
		try
		{
			$sql = "SELECT * FROM $this->table WHERE contact_no='$contactNo'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();	
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}		
	}
	
	public function create_client_bulk($formValues)
	{
		try
		{
			$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			
			if(!empty($_FILES['clientListFile']['name']) && in_array($_FILES['clientListFile']['type'],$file_mimes)) {
				if(is_uploaded_file($_FILES['clientListFile']['tmp_name'])) {
					$csv_file = fopen($_FILES['clientListFile']['tmp_name'], 'r');
					$row = 0;
					while( ($client_record = fgetcsv($csv_file)) !== FALSE ) {
						$row++;
						$srNo = $client_record[0];
						$formValues['ledgers_id'] = "";
						$formValues['wallets_id'] = "";
						$formValues['contact_no'] = $client_record[1];						
						$formValues['client_name'] = $client_record[2];
						$formValues['email_id'] = $client_record[3];
						$formValues['city'] = $client_record[4];
						$formValues['pincode'] = $client_record[5];
						$formValues['address'] = $client_record[6];
						$formValues['dob'] = $client_record[7];
						$formValues['created_date'] = $client_record[8];
						$formValues['generated_by'] = "O";
						
						if( $row > 0 ) {
						
						/*if( $row > 1 && $formValues['contact_no'] != "" ) {
							$check_exist = self::check_exist_client($formValues['contact_no']);
							if($check_exist['status'] == 'error') {
								echo '<script>alert("Brand Already Exist");</script>';
							}
							else {*/
								$add_client_bulk = parent::save($this->table, $formValues);
							//}
							continue;
							
						}
						
						if( !$add_client_bulk ) {
							throw new exception($add_client_bulk);
						}
						
					}
				}	
			}			
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
		
	}
}
?>