<?php
class Delivery_slot extends QTPLDBConfig
{
	var $table = "delivery_slots";
	public $data;
	
	public function check_exist_delivery_slot($deliverySlot)
	{
		try
		{
			$sql = "SELECT * FROM $this->table WHERE delivery_slots='$deliverySlot'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();			
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}
	}
	
	public function add_delivery_slot($formValues)
	{
		try
		{
			$fromTime = isset($formValues['fromTime']) ? $formValues['fromTime'] : "";
			$fromAMPM = isset($formValues['fromAMPM']) ? $formValues['fromAMPM'] : "";
			$toTime = isset($formValues['toTime']) ? $formValues['toTime'] : "";
			$toAMPM = isset($formValues['toAMPM']) ? $formValues['toAMPM'] : "";
			
			$deliverySlot = $fromTime." ".$fromAMPM." - ".$toTime." ".$toAMPM;			
			$deliSlotFrmValues['delivery_slots'] = $deliverySlot;
			
			$check_exist = self::check_exist_delivery_slot($deliSlotFrmValues['delivery_slots']);
			if($check_exist['status'] == 'error') {				
				echo '<script>alert("Delivery Slots Already Exist");</script>';
				//$data = $check_exist;				
			}
			else {
				$add_delivery_slot = parent::save($this->table, $deliSlotFrmValues);
				
				if( !$add_delivery_slot ) {
					throw new exception($add_delivery_slot);
				}
				
				echo '<script>alert("Success");</script>';
				header("location:	delivery_slot.php");
			}			
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>