<?php
class User extends QTPLDBConfig
{
	var $table = "users";
	public $data;
	
	function __construct()
	{
		parent::__construct();
	}
	
	// Check if user already exist
	public function check_user_exist($username, $profileName)
	{
		try {
			$sql = "SELECT username, profile_name FROM $this->table WHERE username='$username' AND profile_name='$profileName'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception("Username already exist");
			}
		}
		catch(Exception $e) {
			$data = array('status'=>'error'); 
			return $data;
		}
	}		
		       
	
	public function login_user($username, $password)
	{
		try {
			$sql = "SELECT * FROM users_login WHERE username='$username' AND password='$password' AND status > '0' LIMIT 1";
			$res = parent::run($sql);			
			$res_login = mysqli_fetch_assoc($res);			
			
			if( isset($res_login) ) {
				$this->login_set = $res_login['users_id'];
				//print_r($login_set);
				return $this->login_set;		
				
				/*$_SESSION['QTPL'] = $res_login;
				$this->user_sess_id = $_SESSION['QTPL']['users_id'];
				$status = $_SESSION['QTPL']['status'];
				print_r($this->user_sess_id);				
				echo '<script>alert("Successfully Login");</script>';
				unset($res_login['password']);				
				//header("location:	index.php");
				return $this->user_sess_id;*/
			}
			if( !$res_login ) {
				throw new exception($res_login);				
			}
		}		
		catch(Exception $e) {
			echo '<script>alert("Login Failed. Please try again.");</script>';
			//header("location:	login.php");
		}
	}
	
	public function check_session()
	{
		try {
			if( !empty($this->login_set) ) {
				echo 'hello';
			}
			else {
				//throw new exception($login_set);
			}
		}
		catch(Exception $e) {
			echo $e;
		}		
	}
	
	public function add_user($formValues)
	{
		try {	
			$userValues['user_type'] = isset($formValues['userType']) ? $formValues['userType'] : "";
			$userValues['designations_id'] = isset($formValues['designation']) ? $formValues['designation'] : "";
			$userValues['first_name'] = isset($formValues['firstName']) ? $formValues['firstName'] : "";
			$userValues['last_name'] = isset($formValues['lastName']) ? $formValues['lastName'] : "";
			$userValues['profile_name'] = isset($formValues['profileName']) ? $formValues['profileName'] : "";
			$userValues['contact_no'] = isset($formValues['contactNo']) ? $formValues['contactNo'] : "";
			$userValues['address'] = isset($formValues['address']) ? $formValues['address'] : "";
			$userValues['gender'] = isset($formValues['gender']) ? $formValues['gender'] : "";
			$userValues['email_id'] = isset($formValues['emailID']) ? $formValues['emailID'] : "";			
			
			switch ($userValues['user_type']) {
				case 'Super Admin':
				$userValues['user_type'] = '1';
				break;
				
				case 'Admin':
				$userValues['user_type'] = '2';
				break;
				
				default:
				$userValues['user_type'] = '3';
				break;
			}
			
			/* #Begin For Photo Upload */
			$src = $_FILES['photo']['tmp_name'];
			$dest = "../theme/imgs/user_imgs/".$userValues['first_name']." ".$userValues['last_name'].".jpg";
			if( move_uploaded_file($src, $dest) ) {
				$userValues['photo'] = $userValues['first_name']." ".$userValues['last_name'].".jpg";
			}
			/* #End For Photo Upload */
			
			$add_user = parent::save($this->table, $userValues);
			
			if( isset($add_user) ) {
				$contact_no = $userValues['contact_no'];
				$profile_name = $userValues['profile_name'];
				
				$sql = "SELECT * FROM $this->table WHERE contact_no='$contact_no' AND profile_name='$profile_name'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$usersID = $res_set['users_id'];
				
				if( !empty($usersID) ) {
					/* #BEGIN Create User Login */
					$userLoginValues['user_type'] = 'U';
					$userLoginValues['users_id'] = $usersID;
					$userLoginValues['username'] = isset($formValues['username']) ? $formValues['username'] : "";
					$userLoginValues['password'] = isset($formValues['password']) ? $formValues['password'] : "";
					$userLoginValues['password'] = base64_encode($formValues['password']);
					
					$add_users_login = parent::save('users_login', $userLoginValues);
					/* #END Create User Login */
					
					/* #BEGIN Create Ledger */
					$ledg_values['users_id'] = '1';
					$ledg_values['ledgers_groups_id'] = '7';
					$ledg_values['ledger_name'] = $userValues['first_name']." ".$userValues['last_name']." A/c";
					$ledg_values['relation_id'] = $usersID;
					$ledg_values['relation_name'] = 'Employee';
					$ledg_values['status'] = '1';
				
					$add_ledgers = parent::save('ledgers', $ledg_values);
					/* #END Create Ledger */
				}
				
				if( $add_ledgers ) {
					$sql = "SELECT * FROM users_login WHERE user_type='U' AND users_id='$usersID'";
					$res = parent::run($sql);
					$res_set = mysqli_fetch_assoc($res);
					$userLoginID = $res_set['users_login_id'];
					
					$sql = "SELECT * FROM ledgers WHERE relation_id='$usersID' AND relation_name='Employee'";
					$res = parent::run($sql);
					$res_set = mysqli_fetch_assoc($res);
					$ledgersID = $res_set['ledgers_id'];
				
					$cond = "users_id='$usersID'";
					$field_name['ledgers_id'] = $ledgersID;
					$field_name['users_login_id'] = $userLoginID;
					
					$update_user = parent::save($this->table, $field_name, $cond);					
				}
					
			}
			else if( !$add_user || !$add_ledgers ) {
				throw new exception($add_ledgers);
			}
			echo '<script>alert("Success");</script>';
			//header("location:	user.php");			
		}
		catch(Exception $e) {
			echo $e;
		}		
	}
	
}
?>