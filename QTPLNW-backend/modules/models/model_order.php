<?php
ini_set('memory_limit','2048M');
class Order extends QTPLDBConfig
{
	var $table = "orders";
	public $data;
	
	/* #BEGIN Online Order */
	public function online_order($formValues)
	{
		try
		{
			$mobileNo = isset($formValues['mobileNo']) ? $formValues['mobileNo'] : "";
			$sql = "SELECT clients_id FROM clients WHERE contact_no='$mobileNo'";
			$res = parent::run($sql);
			$res_set = mysqli_fetch_assoc($res);
			$clients_id = $res_set['clients_id'];
			
			$clientID = isset($formValues['custNo']) ? $formValues['custNo'] : "";
			//$clientFrmValues['client_name'] = isset($formValues['customerName']) ? $formValues['customerName'] : "";
			$clientFrmValues['email_id'] = isset($formValues['emailID']) ? $formValues['emailID'] : "";
			$clientFrmValues['gst_no'] = isset($formValues['gstNo']) ? $formValues['gstNo'] : "";
			$clientFrmValues['address'] = isset($formValues['address']) ? $formValues['address'] : "";
			$clientFrmValues['city'] = isset($formValues['cityName']) ? $formValues['cityName'] : "";
			$clientFrmValues['pincode'] = isset($formValues['pincode']) ? $formValues['pincode'] : "";
			
			$deliverySlots = isset($formValues['deliverySlots']) ? $formValues['deliverySlots'] : "";
			$shippingCharges = isset($formValues['shippingCharges']) ? $formValues['shippingCharges'] : "";			
			$paymentMode = isset($formValues['paymentMode']) ? $formValues['paymentMode'] : "";
			$paymentMode = ($paymentMode == "COD") ? "COD" : "OP";
			$paymentStatus = ($paymentMode == "COD") ? "UP" : "P";
			
			if( $clients_id == $clientID ) {
				$cond = "clients_id='$clients_id' AND contact_no='$mobileNo'";
				$update_client = parent::save('clients', $clientFrmValues, $cond);				
				
				if( $update_client ) {
					$orderFrmValues['order_no'] = "#".uniqid().date('d');
					
					date_default_timezone_set('Asia/Calcutta');
					$orderTime = date('g:i A');
					$date = date('d m Y');
					$date = explode(' ', $date);
					$dd = $date[0];
					$mm = $date[1];
					$yy = $date[2];
                   	$month = gregoriantojd($mm,$dd,$yy);
					$monthName = jdmonthname($month,0);					
					$order_date_time = $dd." ".$monthName." ".$yy." - ".$orderTime;
					$orderFrmValues['order_date_time'] = $order_date_time;
					
					$orderFrmValues['order_date'] = date('Y-m-d');
					$orderFrmValues['order_type'] = "B2C";
					$orderFrmValues['clients_id'] = $clientID;
					$orderFrmValues['contact_no'] = $mobileNo;					
					$orderFrmValues['delivery_slots'] = $deliverySlots;
					$orderFrmValues['sub_total'] = "";
					$orderFrmValues['shipping_charges'] = $shippingCharges;
					$orderFrmValues['wallet_disc'] = "";
					$orderFrmValues['final_total'] = "";
					$orderFrmValues['payment_mode'] = $paymentMode;
					$orderFrmValues['payment_status'] = $paymentStatus;
					$orderFrmValues['generated_by'] = "C";
					
					$add_order = parent::save($this->table, $orderFrmValues);
					
					if( $add_order ) {
						$orderNo = $orderFrmValues['order_no'];
						
						$sql_order = "SELECT orders_id FROM $this->table WHERE clients_id='$clients_id' AND contact_no='$mobileNo' AND order_no='$orderNo' AND order_date_time='$order_date_time'";
						$res_order = parent::run($sql_order);
						$res_set_order = mysqli_fetch_assoc($res_order);
						$newOrderID = $res_set_order['orders_id'];
						
						$productCode = isset($formValues['productCode']) ? $formValues['productCode'] : "";
						$countProduct = count($productCode);
						
						$subTotal = 0;
						for( $index = 0; $index < $countProduct; $index++ ) {
							$prodCode = $_POST['productCode'][$index];
							$orderTempFrmValues['products_id'] = $prodCode;
							$orderTempFrmValues['orders_id'] = $newOrderID;							
							$orderTempFrmValues['product_name'] = $formValues['productName'][$index];
							$orderTempFrmValues['variant'] = $formValues['variantName'][$index];
							$orderTempFrmValues['quantity'] = $formValues['quantity'][$index];
							$orderTempFrmValues['rate'] = $formValues['price'][$index];
							$orderTempFrmValues['amount'] = $formValues['price'][$index] * $formValues['quantity'][$index];
							
							$subTotal = $subTotal + $orderTempFrmValues['amount'];
							
							$add_order_temp = parent::save('order_templates', $orderTempFrmValues);
							
							if( $add_order_temp ) {
								$variant = $formValues['variantName'][$index];
								$firstWord = explode(' ', $variant);
								$variantName = $firstWord[0];
								$measuresName = $firstWord[1];
								$quantity = $formValues['quantity'][$index];
								$productStockValues['stock_sale_online'] = "+".$quantity;
								$productStockValues['stock_bal_online'] = "-".$quantity;
								
								$cond = "product_id='$productCode' AND variant_name='$variantName' AND measures_name='$measuresName'";
								$stock_update = parent::save('variants', $productStockValues, $cond);
							}
							
						}
						
						if( $add_order_temp ) {
							$orderUpdateValues['sub_total'] = $subTotal;
							$fianlTotal = $subTotal + $shippingCharges;
							$orderUpdateValues['final_total'] = $fianlTotal;
							
							$cond = "orders_id='$newOrderID' AND clients_id=$clients_id";
							$update_order = parent::save($this->table, $orderUpdateValues, $cond);
							
							$clientUpdateValues['last_bill_date'] = $order_date_time;
							
							$cond = "clients_id='$clients_id' AND contact_no='$mobileNo'";
							$update_client = parent::save('clients', $clientUpdateValues, $cond);
						}
						
						if( $add_order_temp ) {
							header("location:	order_success.php?orderNo=".$orderNo);
						}
						
					}
				}
			}
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	/* #END Online Order */
	
	public function add_auto_order($formValues)
	{
		try
		{
			$sql = "SELECT clients_id, contact_no FROM clients ORDER BY RAND() LIMIT 0,1";
			$res = parent::run($sql);
			$res_set = mysqli_fetch_assoc($res);
			$orderFrmValues['clients_id'] = $res_set['clients_id'];
			$orderFrmValues['contact_no'] = $res_set['contact_no'];
			//print_r($formValues['clients_id']).'<br>';
			//print_r($formValues['contact_no']);
			
			$orderFrmValues['order_type'] = isset($formValues['orderType']) ? $formValues['orderType'] : "";
			$orderFrmValues['shipping_charges'] = isset($formValues['shippingCharge']) ? $formValues['shippingCharge'] : 0;
			$orderFrmValues['payment_status'] = '1';
			$orderFrmValues['payment_mode'] = '1';
			$orderFrmValues['generated_by'] = 'O';			
			
			$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			
			for( $i = 1; $i <= 4; $i++ ) {
				if(!empty($_FILES['temp'.$i]['name']) && in_array($_FILES['temp'.$i]['type'],$file_mimes) && !empty($formValues['dateTime'.$i]) ) {
					if(is_uploaded_file($_FILES['temp'.$i]['tmp_name'])) {
						$orderFrmValues['order_date_time'] = $formValues['dateTime'.$i];
						$orderDate = explode(' ',$formValues['dateTime'.$i]);
						$orderFrmValues['order_date'] = $orderDate[2]."-".date("m", strtotime($orderDate[1]))."-".$orderDate[0];
						$orderFrmValues['order_no'] = "#".uniqid();
						$orderFrmValues['cilent_order_status'] = "5";
						/* Create Order */
						$add_auto_order = parent::save($this->table, $orderFrmValues);
						/* Create Order */
						
						/* Get New OrderID */
						$clients_id = $orderFrmValues['clients_id'];
						$order_date = $formValues['dateTime'.$i];
						$order_no = $orderFrmValues['order_no'];
						$sql = "SELECT * FROM $this->table WHERE clients_id='$clients_id' AND order_date='$order_date' AND order_no='$order_no'";
						$res = parent::run($sql);
						$res_set = mysqli_fetch_assoc($res);
						$newOrderID = $res_set['orders_id'];
						/* #END Get New OrderID */
						
						$csv_file = fopen($_FILES['temp'.$i]['tmp_name'], 'r');
						$row = 0;
						$subTotal = 0;
						while( ($client_record = fgetcsv($csv_file)) !== FALSE ) {
							$row++;
							$orderTempFrmValues['orders_id'] = $newOrderID;
							$orderTempFrmValues['product_name'] = $client_record[0];
							$orderTempFrmValues['variant'] = $client_record[2];
							$orderTempFrmValues['quantity'] = $client_record[1];
							$orderTempFrmValues['rate'] = $client_record[3];
							$orderTempFrmValues['amount'] = $client_record[4];
							
							$subTotal = $subTotal + $client_record[4];
							
							$add_order_temp = parent::save('order_templates', $orderTempFrmValues);
							continue;
							
							if( !$add_order_temp ) {
								throw new exception($add_order_temp);
							}
						}
						
						$cond = "clients_id='$clients_id' AND orders_id='$newOrderID'";
						$field_name['sub_total'] = $subTotal;
						$field_name['final_total'] = $subTotal + $formValues['shippingCharge'];
						$update_order = parent::save($this->table, $field_name, $cond);
						
					}
				}
			}
			
			if(!$update_order ) {
				echo '<script>alert("Success");</script>';
				header("location:	order.php");
			}
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	
	
	public function update_order($formValues)
	{
		try
		{
			$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			
			if(!empty($_FILES['updateOrderSheet']['name']) && in_array($_FILES['updateOrderSheet']['type'],$file_mimes))
			{
				if(is_uploaded_file($_FILES['updateOrderSheet']['tmp_name'])) {
					$csv_file = fopen($_FILES['updateOrderSheet']['tmp_name'], 'r');
					$row = 0;
					while(($orderUpdate_record = fgetcsv($csv_file)) !== FALSE) {
						$row++;
						$getOrderID = $orderUpdate_record[0];
						$order_date_time = $orderUpdate_record[1];
						$orderDateTime = explode(' ',$order_date_time);
						$orderUpdateFrmValues['order_date'] = $orderDateTime[2]."-".date("m", strtotime($orderDateTime[1]))."-".$orderDateTime[0];
			
						$cond = "orders_id='$getOrderID'";
						
						$updated_order = parent::save($this->table, $orderUpdateFrmValues, $cond);						
					}
				}
			}
						
			if(!$updated_order ) {
				echo '<script>alert("Success");</script>';
				header("location:	order.php");
			}
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>