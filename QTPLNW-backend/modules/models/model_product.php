<?php
class Product extends QTPLDBConfig
{
	var $table = "products";	
	public $data;
	
	public function check_exist_product($productName)
	{
		try
		{
			$sql = "SELECT * FROM $this->table WHERE product_name='$productName'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();			
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}		
	}
	
	public function check_exist_product_variant($productID, $variantName, $mesauresName)
	{
		try
		{
			$sql = "SELECT * FROM variants WHERE products_id='$productID' AND variant_name='$variantName' AND measures_name='$mesauresName'";
			$res = parent::run($sql);
			if( !$res ) {
				throw new exception("Error in query!");
			}
			$cnt = mysqli_num_rows($res);
			if( $cnt > 0 ) {
				throw new exception();			
			}					
		}
		catch(Exception $e)
		{
			$data = array('status'=>'error'); 
			return $data;
		}		
	}
	
	public function add_product($formValues)
	{
		try
		{
			$mainCategoryName = isset($formValues['mainCategoryName']) ? $formValues['mainCategoryName'] : "";
			$subCategoryName = isset($formValues['subCategoryName']) ? $formValues['subCategoryName'] : "";
			$brandName = isset($formValues['brandName']) ? $formValues['brandName'] : "";
			$vendorName = isset($formValues['vendorName']) ? $formValues['vendorName'] : "";
			$productFrmValues['product_name'] = isset($formValues['productName']) ? $formValues['productName'] : "";
			$productFrmValues['hsn_code'] = isset($formValues['hsnCode']) ? $formValues['hsnCode'] : "";
			$productFrmValues['product_type'] = isset($formValues['productType']) ? $formValues['productType'] : "";
			$productFrmValues['product_spec'] = isset($formValues['spec']) ? $formValues['spec'] : "";
			$productFrmValues['product_desc'] = isset($formValues['desc']) ? $formValues['desc'] : "";
			$productFrmValues['keywords'] = isset($formValues['keywords']) ? $formValues['keywords'] : "";
			
			$productFrmValues['categories_id'] = "";
			if( !empty($mainCategoryName) && !empty($subCategoryName) ) {
				$sql = "SELECT * FROM categories WHERE category_type='C' AND category_name='$subCategoryName' AND parent_id='$mainCategoryName'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$productFrmValues['categories_id'] = $res_set['categories_id'];
			}
			
			$productFrmValues['brands_id'] = "";
			if( !empty($brandName) ) {
				$sql = "SELECT * FROM brands WHERE brand_name='$brandName'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$productFrmValues['brands_id'] = $res_set['brands_id'];
			}
			
			$productFrmValues['vendors_id'] = "";
			if( !empty($vendorName) ) {
				$sql = "SELECT * FROM vendors WHERE firm_name='$vendorName'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$productFrmValues['vendors_id'] = $res_set['vendors_id'];
			}
			
			$add_product = parent::save($this->table, $productFrmValues);
			
			if( $add_product ) {
				$product_name = $productFrmValues['product_name'];
				$categories_id = $productFrmValues['categories_id'];
				$vendors_id = $productFrmValues['vendors_id'];
				
				$sql = "SELECT * FROM $this->table WHERE product_name='$product_name' AND categories_id='$categories_id' AND vendors_id='$vendors_id'";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$newProductID = $res_set['products_id'];
				
				$cond = "products_id='$newProductID'";
				
				/* #Begin For Photo Upload */
				for( $i = 1; $i <= 4; $i++ ) {
					if( !empty($_FILES['productImage'.$i]['tmp_name']) ) {
						$src = $_FILES['productImage'.$i]['tmp_name'];
						$dest = "../theme/imgs/product_imgs/".$newProductID."-".$i.".jpg";
						if( move_uploaded_file($src, $dest) ) {
							$field_name['product_img'.$i] = $newProductID."-".$i.".jpg";
						}
					}
				}
				/* #End For Photo Upload */
				
				$update_product = parent::save($this->table, $field_name, $cond);
				
				if( $update_product ) {
					
					for( $index = 1; $index <= 5; $index++ ) {
						if( !empty($formValues['variantName'.$index]) ) {
							$variantFrmValues['variant_name'] = $formValues['variantName'.$index];
							$variantFrmValues['measures_name'] = $formValues['measuresName'.$index];
							$variantFrmValues['purchase_price'] = $formValues['purPrice'.$index];
							$variantFrmValues['mrp'] = $formValues['mrp'.$index];
							$variantFrmValues['btob_price'] = $formValues['btobPrice'.$index];
							$variantFrmValues['btoc_price'] = $formValues['btocPrice'.$index];
							$variantFrmValues['stock_total_online'] = $formValues['stock'.$index];
							$variantFrmValues['products_id'] = $newProductID;
							
							$add_variant = parent::save('variants', $variantFrmValues);
						}
					}
				}
				
			else if( !$add_product ) {
				throw new exception($add_product);
			}
			
			echo '<script>alert("Success");</script>';
			header("location:	product.php");	
			}
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	
	
	public function add_bulk_product($formValues)
	{
		try
		{
			$mainCategoryID = isset($formValues['mainCategoryName']) ? $formValues['mainCategoryName'] : "";
			$subCategoryName = isset($formValues['subCategoryName']) ? $formValues['subCategoryName'] : "";
			$brandID = isset($formValues['brandName']) ? $formValues['brandName'] : "";
			$vendorID = isset($formValues['vendorName']) ? $formValues['vendorName'] : "";
			
			$categoryID = "";
			if( !empty($mainCategoryID) && !empty($mainCategoryID) ) {
				$sql = "SELECT * FROM categories WHERE category_type='C' AND category_name='$subCategoryName' AND parent_id='$mainCategoryID' LIMIT 1";
				$res = parent::run($sql);
				$res_set = mysqli_fetch_assoc($res);
				$categoryID = $res_set['categories_id'];					
			}
			
			$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			
			if( !empty($_FILES['productListFile']['name']) && in_array($_FILES['productListFile']['type'],$file_mimes) )
			{
				if( is_uploaded_file($_FILES['productListFile']['tmp_name']) ) {
					$csv_file = fopen($_FILES['productListFile']['tmp_name'], 'r');
					$row = 0;
					while( ($product_record = fgetcsv($csv_file)) !== FALSE )
					{
						$row++;
						$srNo = $product_record[0];
						
						$productName = $product_record[1];
						$productName = trim($productName);
						
						$hsnCode = $product_record[2];
						$hsnCode = trim($hsnCode);
						
						$productType = $product_record[3];
						$productType = trim($productType);
						
						$specification = $product_record[4];
						$specification = trim($specification);
						
						$description = $product_record[5];
						$description = trim($description);
						
						$keywords = $product_record[6];
						
						$gstRate = $product_record[7];
						$cgstRate = 0;
						$sgstRate = 0;
						if( $gstRate > 0 || !empty($gstRate) ) {
							$cgstRate = $gstRate / 2;
							$sgstRate = $gstRate / 2;						
						}
						
						if( $row > 1 && $productName != "" ) {
							$productFrmValues['categories_id'] = $categoryID;
							$productFrmValues['vendors_id'] = $vendorID;
							$productFrmValues['brands_id'] = $brandID;
							$productFrmValues['product_type'] = $productType;
							$productFrmValues['product_name'] = $productName;
							$productFrmValues['hsn_code'] = $hsnCode;
							$productFrmValues['keywords'] = $keywords;
							$productFrmValues['product_desc'] = $description;
							$productFrmValues['product_spec'] = $specification;
							$productFrmValues['cgst_rate'] = $cgstRate;
							$productFrmValues['sgst_rate'] = $sgstRate;
							
							$check_exist = self::check_exist_product($productName);
							if($check_exist['status'] == 'error') {
							/*echo '<script>alert("Brand Already Exist");</script>';*/
							//$data = $check_exist;
							}
							else {
								$add_bulk_product = parent::save($this->table, $productFrmValues);
								
								if( !$add_bulk_product ) {
									throw new exception($add_bulk_product);
								}
							}
							
						}
					}					
				}
			}
			/* #BEGIN Variant Add */
			if( !empty($_FILES['variantListFile']['name']) && in_array($_FILES['variantListFile']['type'],$file_mimes) )
			{
				if( is_uploaded_file($_FILES['variantListFile']['tmp_name']) ) {
					$csv_file = fopen($_FILES['variantListFile']['tmp_name'], 'r');
					$row = 0;
					while( ($variant_record = fgetcsv($csv_file)) !== FALSE )
					{
						$row++;
						$srNo = $variant_record[0];
						
						$product_name = $variant_record[1];
						$product_name = trim($product_name);
						
						$sql = "SELECT * FROM $this->table WHERE product_name='$product_name' LIMIT 1";
						$res = parent::run($sql);
						$res_set = mysqli_fetch_assoc($res);
						$productID = $res_set['products_id'];
						
						if( $productID == "" || empty($productID) ) {
							$productID = 0;
						}
						
						$variantName = $variant_record[2];
						$variantName = trim($variantName);
						
						$measuresName = $variant_record[3];
						$measuresName = trim($measuresName);
						
						$purPrice = $variant_record[4];
						$mrp = $variant_record[5];
						$btob = $variant_record[6];
						$btoc = $variant_record[7];
						$stock = $variant_record[8];
						
						if( $row > 1 && $productID != 0 && $product_name != "" && $variantName != "" ) {
							$variantFrmValues['products_id'] = $productID;
							$variantFrmValues['variant_name'] = $variantName;
							$variantFrmValues['measures_name'] = $measuresName;
							$variantFrmValues['purchase_price'] = $purPrice;
							$variantFrmValues['mrp'] = $mrp;
							$variantFrmValues['btob_price'] = $btob;
							$variantFrmValues['btoc_price'] = $btoc;
							
							$check_exist = self::check_exist_product_variant($productID, $variantName, $measuresName);
							if($check_exist['status'] == 'error') {
							/*echo '<script>alert("Brand Already Exist");</script>';*/
							//$data = $check_exist;
							}
							else {
								$add_bulk_variant = parent::save('variants', $variantFrmValues);
								
								if( !$add_bulk_variant ) {
									throw new exception($add_bulk_variant);
								}
							}						
						}
					}
				}
			}
			/* #END Variant Add */	
			//}
			/*if( !$add_bulk_variant ) {
				throw new exception($add_bulk_variant);
			}*/
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	
	
}
?>
