<?php
class Banner extends QTPLDBConfig
{
	var $table = "banners";
	
	public function add_banner($formValues)
	{
		try
		{
			$vendorFrmValues['banner_link'] = isset($formValues['bannerLink']) ? $formValues['bannerLink'] : "";
			$vendorFrmValues['button_name'] = isset($formValues['buttonName']) ? $formValues['buttonName'] : "";
			$vendorFrmValues['button_link'] = isset($formValues['buttonLink']) ? $formValues['buttonLink'] : "";
			$vendorFrmValues['sort_order'] = isset($formValues['sortOrder']) ? $formValues['sortOrder'] : "";
			
			/* #Begin For Photo Upload */
			//$imgType = $_FILES['bannerImage']['type'];
			$src = $_FILES['bannerImage']['tmp_name'];
			//if( $imgType != '.jpg' ) {
			$dest = "../theme/imgs/banner_imgs/".$_FILES['bannerImage']['name'];
			if( move_uploaded_file($src, $dest) ) {
				$vendorFrmValues['banner_image'] = $_FILES['bannerImage']['name'];
			}
			//}
			/* #End For Photo Upload */
			$add_banner = parent::save($this->table, $vendorFrmValues);
			
			if( !$add_banner ) {
				throw new exception($add_banner);
			}
			echo '<script>alert("Success");</script>';
			header("location:	banner.php");
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>