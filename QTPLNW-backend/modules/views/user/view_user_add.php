<div class="card card-primary">
	<div class="card-header col-sm-12">
    	<div class="col-sm-8 pl"><label>Create New User</label></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
    </div>
    <div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">
    	<div class="stepwizard col-md-offset-3">
        	<div class="stepwizard-row setup-panel">
            	<div class="stepwizard-step"><a href="#step-1" type="button" class="btn btn-primary">User Type</a></div>
                <div class="stepwizard-step"><a href="#step-2" type="button" class="btn btn-default" disabled="disabled">Personal Info.</a></div>
                <div class="stepwizard-step"><a href="#step-3" type="button" class="btn btn-default" disabled="disabled">Login Details</a></div>
            </div>
        </div>    
    </div>
    <form name="addNewUserForm" action="" method="post" enctype="multipart/form-data">
    <div class="row setup-content" id="step-1">
    	<div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">User Type :</label></div>
            <div class="col-sm-9 pr">
            	<label class="col-sm-4"><input type="radio" name="userType" value="Super Admin" required /> Super Admin</label>
                <label class="col-sm-3"><input type="radio" name="userType" value="Admin" required /> Admin</label>
                <label class="col-sm-4"><input type="radio" name="userType" value="Sub Admin" required /> Sub Admin</label>
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Designation :</label></div>
            <div class="col-sm-8 pl">
            	<select name="designation" class="form-control" required>
                	<option value="" selected> --- Designation --- </option>
                    <?php
					foreach ($designation_list as $designation) {
					?>
                    	<option value="<?php echo $designation['designations_id']; ?>"><?php echo $designation['name']; ?></option>
                    <?php
					}
					?>
                </select>
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="form-group">
            	<button class="btn btn-primary nextBtn pr" type="button" >Next</button>
            </div>
        </div>
    </div>
    
    <div class="row setup-content" id="step-2">
    	<div class="form-group col-sm-12">
       		<div class="col-sm-3 pl"><label class="control-label">Full Name :</label></div>
            <div class="col-sm-8 pl">
            	<input  type="text" name="firstName" class="form-control col-sm-5 pl" required placeholder="First Name" />
                <input  type="text" name="lastName" class="form-control col-sm-5 pr" required placeholder="Last Name" />
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Profile Name :</label></div>
            <div class="col-sm-9 pl">
            	<input  type="text" name="profileName" class="form-control" required placeholder="Profile Name" />
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Contact No :</label></div>
            <div class="col-sm-9 pl">
            	<input  type="number" name="contactNo" maxlength="10" class="form-control col-sm-6" required placeholder="Contact No." />
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Address :</label></div>
            <div class="col-sm-9 pl">
            	<textarea name="address" class="form-control" placeholder="Address"></textarea>
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Gender :</label></div>
            <div class="col-sm-9 pl">
            	<label class="col-sm-4"><input type="radio" name="gender" value="M" /> Male</label>
                <label class="col-sm-3"><input type="radio" name="gender" value="F" /> Female</label>
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Photo :</label></div>
            <div class="col-sm-9 pl"><input type="file" name="photo" /></div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="form-group">
            	<button class="btn btn-primary nextBtn pr" type="button" >Next</button>
            </div>
        </div>
    </div>
    
    <div class="row setup-content" id="step-3">
    	<div class="card-footer col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Email ID :</label></div>
            <div class="col-sm-8 pl">
            	<input type="email" name="emailID" class="form-control" required placeholder="Email ID" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Username :</label></div>
            <div class="col-sm-8 pl">
            	<input type="text" name="username" class="form-control" required maxlength="10" placeholder="Username" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Password :</label></div>
            <div class="col-sm-8 pl">
            	<input type="password" name="password" class="form-control" required placeholder="Password" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="form-group">
            	<button type="submit" name="addNewUser" class="btn btn-success pr">Submit</button>
            </div>
        </div>
    </div>
    
    </form>
</div>