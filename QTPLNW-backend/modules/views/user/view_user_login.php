<div class="col-sm-5" style="margin:150px auto">
<form name="userLoginForm" action="" method="POST" enctype="multipart/form-data">
<div class="card card-primary">
	<div class="card-header">
    	<div class="col-sm-4 pl"><span class="ard-title"></span>Login Form</div>
    </div>
    <div class="card-body">    	
        <div class="form-group col-sm-12 row">
        	<div class="col-sm-3 pl"><label class="control-label">Username :</label></div>
            <div class="col-sm-9 pr"><input type="text" name="username" value="" class="form-control" required tabindex="1" /></div>
        </div>
        <div class="form-group col-sm-12 row">
        	<div class="col-sm-3 pl"><label class="control-label">Password :</label></div>
            <div class="col-sm-9 pr"><input type="password" name="password" value="" class="form-control" required /></div>
        </div>
    </div>
    <div class="card-footer">
    	<input type="reset" value="RESET" class="btn btn-danger" />
    	<input type="submit" name="userLogin" value="LOGIN" class="btn btn-success pr" />
    </div>
</div>
</form>
</div>