<?php 
$User = new User;
$user_list = $User->select($User->table,'');
$Designation = new Designation;
$designation_list = $Designation->select($Designation->table,'');
?>
<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>User List</span></div>
            <div class="col-sm-5 pl"><input type="search" class="form-control form-control-navbar light-table-filter" data-table="order-table" placeholder="Search" aria-label="Search"></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewUser">Add New User</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Photo</th>
				<th>Designation <?php $User->check_session(); ?></th>
				<th>Name</th>
				<th>Profile Name</th>
				<th>Email ID</th>
				<th>Contact No.</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$sr_no = 1;
			foreach( $user_list as $user )
			{
				//$password = $user['password'];
				$photo = $user['photo'];
				$photo = '<img src="../theme/imgs/user_imgs/'.$photo.'" width="40px" height="40px" class="img-circle" />';
				$designation_name = "";
				foreach( $designation_list as $designation) {
					if( $designation['designations_id'] == $user['designations_id'] ) {
						$designation_name = $designation['name'];
					}
				}	
				//$ps = base64_decode($password);			
				?>
			<tr>
				<th><?php echo($sr_no); ?></th>
				<td><?php echo $photo; ?></td>
				<td><?php echo $designation_name; ?></td>
				<td><?php echo $user['first_name']." ".$user['last_name']; ?></td>
				<td><?php echo $user['profile_name']; ?></td>
				<td><?php echo $user['email_id']; ?></td>
				<td><?php echo $user['contact_no']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
					<span class="btn-info btn-key"><i class="fa fa-key"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
			}
			?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate col-md-6 col-sm-12" id="modal-addNewUser">
	<?php include('modules/views/user/view_user_add.php'); ?>
</div>
