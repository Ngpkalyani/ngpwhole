<div class="card card-primary">
<form name="formAddNewShippingCharge" id="formAddNewShippingCharge" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create Pincode</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-5 pl"><label class="control-label">Pincode *</label></div>
				<div class="col-sm-6 pl">
					<input type="number" name="pincode" value="<?php echo isset($_POST['pincode']) ? $_POST['pincode'] : ""; ?>" required placeholder="Pincode" maxlength="6" class="form-control" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-5 pl"><label class="control-label">Order Amount *</label></div>
				<div class="col-sm-6 pl">
					<input type="number" name="orderAmount" value="<?php echo isset($_POST['orderAmount']) ? $_POST['orderAmount'] : ""; ?>" required placeholder="Order Amount" class="form-control" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-5 pl"><label class="control-label">Shipping Charges *</label></div>
				<div class="col-sm-6 pl">
					<input type="number" name="shippingCharges" value="<?php echo isset($_POST['shippingCharges']) ? $_POST['shippingCharges'] : ""; ?>" required placeholder="Charges Amount" maxlength="3" class="form-control" />
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addNewShippingCharge" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>
