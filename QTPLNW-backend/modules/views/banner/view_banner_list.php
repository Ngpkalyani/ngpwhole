<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Category List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar light-table-filter" type="search" data-table="order-table" placeholder="Search" /></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewBanner">Add New Banner</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Banner Image</th>
				<th>Banner Link</th>
				<th>Button Name</th>
				<th>Button Link</th>
				<th>Sort Order</th>
                <th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $banner_list as $banner )
		{
			$banner_imgs = $banner['banner_image'];
			$bannerImage = '<img src="theme/imgs/banner_imgs/'.$banner_imgs.'" width="100px" height="50px" />';
			?>
			<tr>
            	<th><?php echo($sr_no); ?></th>
				<td><?php echo $bannerImage; ?></td>
				<td><?php echo $banner['banner_link']; ?></td>
				<td><?php echo $banner['button_name']; ?></td>
				<td><?php echo $banner['button_link']; ?></td>
				<td><?php echo $banner['sort_order']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
		}
			?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate col-md-6 col-sm-12" id="modal-addNewBanner">
	<?php include('modules/views/banner/view_banner_add.php'); ?>
</div>
