<div class="card card-primary">
<form name="formAddNewBanner" id="formAddNewBanner" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Add New Banner</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label class="control-label">Banner Image *</label></div>
				<div class="col-sm-8 pl">
					<input type="file" name="bannerImage" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Banner Link *</label></div>
				<div class="col-sm-8 pr">
					<textarea name="bannerLink" class="form-control" placeholder="Banner Link"></textarea>
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Button Name *</label></div>
				<div class="col-sm-8 pr">
					<input type="name" name="buttonName" value="<?php echo isset($_POST['buttonName']) ? $_POST['buttonName'] : ""; ?>" required class="form-control" placeholder="Button Name" />
				</div>
			</div>
			<div class="form-group col-sm-12 row">
            	<div class="col-sm-4 pl"><label>Button Link *</label></div>
				<div class="col-sm-8 pl">
					<textarea name="buttonLink" class="form-control" placeholder="Button Link"></textarea>
				</div>
			</div>
            <div class="form-group col-sm-12 row">
            	<div class="col-sm-4 pl"><label>Sort Order*</label></div>
				<div class="col-sm-4 pl">
					<input type="number" name="sortOrder" value="<?php echo isset($_POST['sortOrder']) ? $_POST['sortOrder'] : ""; ?>" required class="form-control" />
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addNewBanner" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>