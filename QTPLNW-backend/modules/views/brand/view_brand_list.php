<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Brand List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar light-table-filter" type="search" data-table="order-table" placeholder="Search" /></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewBrand">Add New Brand</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Image</th>
				<th>Brand Name</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $brand_list as $brand )
		{
			$imgs = $brand['imgs'];
			$brandImages = '<img src="theme/imgs/brand_imgs/'.$imgs.'" width="40px" height="40px" class="img-circle" />';
			?>
			<tr>
            	<th><?php echo($sr_no); ?></th>
				<td><?php echo $brandImages; ?></td>
				<td><?php echo $brand['brand_name']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<a data-href="brand_edit.php?brandID=<?php echo $brand['brands_id']; ?>" class="modal-editBrand"><span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span></a> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
		}
			?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate col-md-5 col-sm-12" id="modal-addNewBrand">
	<?php include('modules/views/brand/view_brand_add.php'); ?>
</div>

<div class="modal animate col-md-5 col-sm-12" id="modal-editBrand" style="height:auto">
	<div class="modal-editBrand-content">
    </div>
</div>