<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Category List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar light-table-filter" type="search" data-table="order-table" placeholder="Search" /></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewCategory">Add New Category</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Image</th>
				<th>Main Category</th>
				<th>Sub Category</th>
				<th>Count Sub Category</th>
				<th>Count Products</th>
				<th>Sort Order</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $category_list as $category )
		{
			$imgs = $category['imgs'];
			$categoryImages = '<img src="../theme/imgs/category_imgs/'.$imgs.'" width="40px" height="40px" class="img-circle" />';
			$parent_category = "";
			$child_category = "";
			if( $category['parent_id'] != 0 ) {
				$child_category = $category['category_name'];
				$parent_category_id = $category['parent_id'];
				
				$sql = "SELECT category_name FROM categories WHERE categories_id='$parent_category_id'";
				$res = $Category->run($sql);
				$res_category = mysqli_fetch_assoc($res);
				$parent_category = $res_category['category_name'];
			}
			else {
				$parent_category = $category['category_name'];
			}
			?>
			<tr>
            	<th><?php echo($sr_no); ?></th>
				<td><?php echo $categoryImages; ?></td>
				<td><?php echo $parent_category; ?></td>
				<td><?php echo $child_category; ?></td>
				<td><?php //echo $category['sort_order']; ?></td>
				<td><?php //echo $categoryImages; ?></td>
				<td><?php echo $category['sort_order']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<a data-href="category_edit.php?categoryID=<?php echo $category['categories_id']; ?>" class="modal-editCategory"><span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span></a> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
		}
			?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate col-md-6 col-sm-12" id="modal-addNewCategory">
	<?php include('modules/views/category/view_category_add.php'); ?>
</div>

<div class="modal animate col-md-6 col-sm-12" id="modal-editCategory" style="height:auto">
	<div class="modal-editCategory-content">
    </div>
</div>
