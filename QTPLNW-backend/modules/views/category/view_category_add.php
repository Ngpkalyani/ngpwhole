<div class="card card-primary">
<form name="formAddNewCategory" id="formAddNewCategory" method="post" action="" onsubmit="return valid_categoryForm();" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create New Category</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label class="control-label">Category Name *</label></div>
				<div class="col-sm-8 pl">
					<input type="text" name="categoryName" value="<?php echo isset($_POST['categoryName']) ? $_POST['categoryName'] : ""; ?>" required placeholder="Category Name" class="form-control" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Category Image *</label></div>
				<div class="col-sm-8 pr">
					<input type="file" name="categoryImage" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Sort Order *</label></div>
				<div class="col-sm-3 pr">
					<input type="number" name="sortOrder" value="<?php echo isset($_POST['sortOrder']) ? $_POST['sortOrder'] : ""; ?>" required class="form-control" maxlength="2" />
				</div>
			</div>
			<div class="form-group col-sm-12 row">
            	<div class="col-sm-4 pl"><label>Category Type *</label></div>
				<div class="col-sm-4 pl">
					<label><input type="radio" name="categoryType" value="P"<?PHP echo isset($_POST['categoryType']) && $_POST['categoryType'] == 'P' ? 'checked' : ""; ?> id="parentCategory" /> As a Parent Category</label>
				</div>
				<div class="col-sm-4 pl">
					<label><input type="radio" name="categoryType" value="C"<?PHP echo isset($_POST['categoryType']) && $_POST['categoryType'] == 'C' ? 'checked' : ""; ?> id="childCategory" /> As a Child Category</label>
				</div>
			</div>
			<div class="form-group col-sm-12 row" style="display:none" id="parentCatgField">
				<div class="col-sm-4 pl"><label>Parent Category *</label></div>
				<div class="col-sm-8 pl">
					<select name="parentCategoryName" class="form-control select parentCategoryName">
                    	<option value="<?php echo isset($_POST['parentCategoryName']) ? $_POST['parentCategoryName'] : ""; ?>" selected> --- Parent Category --- <?php echo isset($_POST['parentCategoryName']) ? $_POST['parentCategoryName'] : ""; ?></option>
                        <?php
						foreach( $category_list as $category ) {
							$category_name = $category['category_name'];
							if( $category['category_type'] == 'P' && $category['parent_id'] == '0' && $category['status'] > 0  ) {
								?>
                                <option value="<?php echo $category_name; ?>"><?php echo $category_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addNewCategory" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>

