<?php
$LedgersGroups = new LedgersGroups;
$ledgers_groups_list = $LedgersGroups->select($LedgersGroups->table,'');
$Ledgers = new Ledgers;
$ledgers_list = $Ledgers->select($Ledgers->table,'');
?>
<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Ledgers List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewLedger">Add New Ledger</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Ledgers Groups</th>
				<th>Ledgers Name</th>
				<th>Debit Balance</th>
				<th>Credit Balance</th>
                <th>Balance Amt.</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $ledgers_groups_list as $ledgers_groups )
		{
			?>
			<tr>
				<th><?php echo($sr_no); ?></th>
				<td><?php echo $ledgers_groups['ledgers_name']; ?></td>
				<td><?php //echo $ledgers_groups['ledgers_Name']; ?></td>
				<td><?php //echo $ledgers_groups['ledgers_Name']; ?></td>
				<td><?php //echo $ledgers_groups['ledgers_Name']; ?></td>
                <td><?php //echo $ledgers_groups['ledgers_Name']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
		<?php
		$sr_no++;
		}
		?>
		<?php
		foreach( $ledgers_list as $ledgers )
		{
			$ledgers_groups_name = "";
			foreach( $ledgers_groups_list as $ledgers_groups ) {
				if( $ledgers_groups['ledgers_groups_id'] == $ledgers['ledgers_groups_id'] ) {
					$ledgers_groups_name = $ledgers_groups['ledgers_name'];
				}
			}
			?>
			<tr>
				<th><?php echo($sr_no); ?></th>
				<td><?php echo $ledgers_groups_name; ?></td>
				<td><?php echo $ledgers['ledger_name']; ?></td>
				<td></td>
				<td></td>
                <td></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
		<?php
		$sr_no++;
		}
		?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate" id="modal-addNewLedger" style="width: 40%">
	<?php include('modules/views/ledgers/view_ledgers_add.php'); ?>
</div>