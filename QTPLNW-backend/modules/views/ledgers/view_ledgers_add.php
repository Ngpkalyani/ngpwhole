<div class="card card-primary">
<form name="formAddNewLedger" id="formAddNewLedger" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create New Ledger</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<label class="col-sm-4">Ledger Name *</label>
				<div class="col-sm-8">
					<input type="text" name="ledgerName" value="<?php echo isset($_POST['ledgerName']) ? $_POST['ledgerName'] : ""; ?>" required placeholder="Ledger Name" class="form-control" />
				</div>
			</div>
			<div class="form-group col-sm-12 row">
				<div class="col-sm-6">
					<label><input type="radio" name="ledgerType" value="P"<?PHP echo isset($_POST['ledgerType']) && $_POST['ledgerType'] == 'P' ? 'checked' : ""; ?> id="parentLedger" /> As a Parent Ledger</label>
				</div>
				<div class="col-sm-6">
					<label><input type="radio" name="ledgerType" value="C"<?PHP echo isset($_POST['ledgerType']) && $_POST['ledgerType'] == 'C' ? 'checked' : ""; ?> id="childLedger" /> As a Child Ledger</label>
				</div>
			</div>
			<div class="form-group col-sm-12 row" style="display:none" id="parentLedgField">
				<label class="col-sm-4">Ledgers Group *</label>
				<div class="col-sm-8">
					<select name="ledgerGroup" class="form-control select ledgerGroup">
                    	<option value="<?php echo isset($_POST['ledgerGroup']) ? $_POST['ledgerGroup'] : ""; ?>" selected>( Select Ledgers Group )<?php echo isset($_POST['ledgerGroup']) ? $_POST['ledgerGroup'] : ""; ?></option>
                        <?php
						foreach( $ledgers_groups_list as $ledgers_groups ) {
							$ledgerGroup = $ledgers_groups['ledgers_Name'];
							if( $ledgers_groups['status'] > 0 ) {
								?>
                                <option value="<?php echo $ledgerGroup; ?>"><?php echo $ledgerGroup; ?></option>
                                <?php
							}
						}
						?>
                    </select>
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addNewLedger" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>

