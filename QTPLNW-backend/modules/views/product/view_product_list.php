<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Product List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"></div>
            <div class="col-sm-2 pl"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addBulkProduct">Add Bulk Products</span></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewProduct">Add New Product</span></div>
        </div>
    </div>
	<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th>#</th>
                <th>Prod. Imgs</th>
				<th>Main Category</th>
				<th>Sub Category</th>
                <th>Brand Name</th>
				<th>Product Name</th>
                <th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $product_list as $product )
		{
			$imgs = $product['product_img1'];
			$ProductImages = '<img src="../theme/imgs/product_imgs/'.$imgs.'" width="40px" height="40px" class="img-circle" />';
			
			$sql = "SELECT * FROM categories WHERE categories_id='".$product['categories_id']."'";
			$res = $Category->run($sql);
			$res_category = mysqli_fetch_assoc($res);
			$child_category = $res_category['category_name'];
			$parentCategoryID = $res_category['parent_id'];
			
			$sql = "SELECT * FROM categories WHERE categories_id='$parentCategoryID'";
			$res = $Category->run($sql);
			$res_category = mysqli_fetch_assoc($res);
			$parent_category = $res_category['category_name'];
			
			
			$sql = "SELECT brand_name FROM brands WHERE brands_id='".$product['brands_id']."'";
			$res = $Category->run($sql);
			$res_category = mysqli_fetch_assoc($res);
			$brand_name = $res_category['brand_name'];
			?>
			<tr>
				<th><?php echo($sr_no); ?></th>
                <th><?php echo $ProductImages; ?></th>
				<td><?php echo $parent_category; ?></td>
				<td><?php echo $child_category; ?></td>
                <td><?php echo $brand_name; ?></td>
				<td><?php echo $product['product_name']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
        <?php
		$sr_no++;
		}
		?>
		</tbody>
	</table>	
	</div>
</div>

<div class="modal animate col-md-8 col-sm-12" id="modal-addNewProduct">
	<?php include('modules/views/product/view_product_add.php'); ?>
</div>

<div class="modal animate col-md-8 col-sm-12" id="modal-addBulkProduct">
	<?php include('modules/views/product/view_bulk_product_add.php'); ?>
</div>
