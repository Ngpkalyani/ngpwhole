<div class="card card-primary">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create New Product</label></div>
			<div class="col-sm-3"></div>
        	<div class="col-sm-1 pr"><span class="btn btn-danger btn-sm pr" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">
        	<div class="stepwizard col-md-offset-3">
        		<div class="stepwizard-row setup-panel">
            		<div class="stepwizard-step"><a href="#step-1" type="button" class="btn btn-primary">Product Details</a></div>
                	<div class="stepwizard-step"><a href="#step-2" type="button" class="btn btn-default" disabled="disabled">Variant</a></div>
            	</div>
        	</div>
        </div>
        <form name="addNewProductForm" action="" method="post" enctype="multipart/form-data">
        <!-- Tab 1 Start -->
        <div class="row setup-content" id="step-1">
        	<div class="form-group col-sm-12">
				<div class="col-sm-4 pl">
                	<label>Main Category *</label>
                    <select name="mainCategoryName" class="form-control select" onchange="getSubCategory(this.value);" required>
                    	<option value="<?php echo isset($_POST['mainCategoryName']) ? $_POST['mainCategoryName'] : ""; ?>" selected> --- Main Category --- <?php echo isset($_POST['mainCategoryName']) ? $_POST['mainCategoryName'] : ""; ?></option>
                        <?php
						foreach( $category_list as $category ) {
							$category_name = $category['category_name'];
							$categoryID = $category['categories_id'];
							if( $category['category_type'] == 'P' && $category['parent_id'] == '0' && $category['status'] > 0  ) {
								?>
                                <option value="<?php echo $categoryID; ?>"><?php echo $category_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                </div>
                <div class="col-sm-4 pl">
                	<label>Sub Category *</label>
                    <select name="subCategoryName" class="form-control select subCategoryName" required>
                    	<option value="<?php echo isset($_POST['subCategoryName']) ? $_POST['subCategoryName'] : ""; ?>" selected> --- Sub Category --- <?php echo isset($_POST['subCategoryName']) ? $_POST['subCategoryName'] : ""; ?></option>
                    </select>
                </div>
                <div class="col-sm-4 pl">
                	<label>Brand Name *</label>
                    <select name="brandName" class="form-control select">
                    	<option value="<?php echo isset($_POST['brandName']) ? $_POST['brandName'] : ""; ?>" selected> --- Brand Name --- <?php echo isset($_POST['brandName']) ? $_POST['brandName'] : ""; ?></option>
                        <?php
						foreach( $brand_list as $brand ) {
							$brand_name = $brand['brand_name'];
							if( $brand['status'] > 0  ) {
								?>
                                <option value="<?php echo $brand_name; ?>"><?php echo $brand_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                </div>                				
			</div>            
            <div class="form-group col-sm-12">
            	<div class="col-sm-4 pl">
                	<label>Vendor Name *</label>
                    <select name="vendorName" class="form-control select" required>
                    	<option value="<?php echo isset($_POST['vendorName']) ? $_POST['vendorName'] : ""; ?>" selected> --- Vendor Name --- <?php echo isset($_POST['vendorName']) ? $_POST['vendorName'] : ""; ?></option>
                        <?php
						foreach( $vendor_list as $vendor ) {
							$firm_name = $vendor['firm_name'];
							if( $vendor['status'] > 0  ) {
								?>
                                <option value="<?php echo $firm_name; ?>"><?php echo $firm_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                </div>
            	<div class="col-sm-4 pl">
                	<label>Product Name *</label>
                    <input type="text" name="productName" value="<?php echo isset($_POST['productName']) ? $_POST['productName'] : ""; ?>" required placeholder="Product Name" class="form-control" />                    
                </div>                
                <div class="col-sm-2 pl">
                	<label>HSN Code</label>
                    <input type="text" name="hsnCode" value="<?php echo isset($_POST['hsnCode']) ? $_POST['hsnCode'] : ""; ?>"  placeholder="HSN Code" class="form-control" />                    
                </div>
                <div class="col-sm-2 pl">
                	<label>Product Type</label>
                    <select name="productType" class="form-control select" required>
                    	<option value="<?php echo isset($_POST['productType']) ? $_POST['productType'] : ""; ?>" selected>Prod. Type<?php echo isset($_POST['productType']) ? $_POST['productType'] : ""; ?></option>
                        <option value="B2B">B2B</option>
                        <option value="B2C">B2C</option>
                        <option value="BOTH">BOTH</option>
                    </select>                    
                </div>                			
			</div>
            <div class="form-group col-sm-12">
            	<div class="col-sm-3 pl">
                	<label for="exampleInputFile">Product Image 1 *</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="productImage1" class="form-control" required>
                      </div>
                    </div>
                </div>
                <div class="col-sm-3 pl">
                	<label for="exampleInputFile">Product Image 2</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="productImage2" class="form-control" />
                      </div>
                    </div>                   
                </div>
                <div class="col-sm-3 pl">
                	<label for="exampleInputFile">Product Image 3</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="productImage3" class="form-control" />
                      </div>
                    </div>                   
                </div>
                <div class="col-sm-3 pl">
                	<label for="exampleInputFile">Product Image 4</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="productImage4" class="form-control" />
                      </div>
                    </div>                   
                </div>
            </div>					
			<div class="form-group col-sm-12">
            	<div class="col-sm-4 pl">
                	<label>Specification</label>
                    <textarea name="spec" class="form-control" placeholder="Specification"></textarea>                    
                </div>
                <div class="col-sm-4 pl">
                	<label>Description</label>
                    <textarea name="desc" class="form-control" placeholder="Description"></textarea>                    
                </div>
                <div class="col-sm-4 pl">
                	<label>Keywords *</label>
                    <textarea name="keywords" class="form-control" placeholder="Keywords1, Keywords2," required></textarea>                    
                </div>
			</div>
            <div class="form-group col-sm-12">
            	<div class="col-sm-4 pl">
                	<label>Min Stock Limit</label>
                    <input type="number" name="minStockLimit" value="<?php echo isset($_POST['minStockLimit']) ? $_POST['minStockLimit'] : ""; ?>" placeholder="Stock Limit" class="form-control" />                    
                </div>
                <div class="col-sm-4 pl">
                	<label>Profit on Product (%)</label>
                    <input type="text" name="productProfit" value="<?php echo isset($_POST['productProfit']) ? $_POST['productProfit'] : ""; ?>" placeholder="Vendors Profit" class="form-control" />
                </div>
                <div class="col-sm-4 pl">
                	<label>GST Rate</label>
                    <div class="col-sm-12">
                    <input type="text" name="cgstRate" value="<?php echo isset($_POST['cgstRate']) ? $_POST['cgstRate'] : ""; ?>"  placeholder="CGST" class="form-control col-sm-5 pl" /> 
                    <input type="text" name="sgstRate" value="<?php echo isset($_POST['sgstRate']) ? $_POST['sgstRate'] : ""; ?>"  placeholder="SGST" class="form-control col-sm-5 pr" />
                    </div>                   
                </div>
			</div>
            <div class="card-footer col-sm-12">
        		<div class="form-group">
            		<button class="btn btn-primary nextBtn pr" type="button" >Next</button>
            	</div>
        	</div>
		</div>
        <!-- Tab 1 End -->
        <!-- Tab 2 Start -->
        <div class="row setup-content div_row" id="step-2">
        	<div class="form-group col-sm-12 row">
          <table class="table table-bordered table-hover table-striped">
          	<thead>
            	<tr>
                	<th>#</th>
                    <th>Variant Name</th>
                    <th>Measures</th>
                    <th>Pur Price</th>
                    <th>MRP</th>
                    <th>B2B Price</th>
                    <th>B2C Price</th>
                    <th>Stock</th>
                </tr>
            </thead>
            <tbody>
            <?php
			$i = 1;  
			while( $i <= 5 ) {
				?>
                <tr>
                	<th><?php echo($i); ?></th>
                    <td><input type="text" name="variantName<?php echo($i); ?>" value="" placeholder="Variant Name" class="form-control" id="variantName" /></td>
                    <td>
                    <select name="measuresName<?php echo($i); ?>" class="form-control select" >
                    	<option value="" selected>Measures</option>
                        <?php
						foreach( $measures_list as $measures ) {
							$measures_name = $measures['measures_name'];
							$measures_id = $measures['measures_name'];
							if( $category['status'] > 0  ) {
								?>
                                <option value="<?php echo $measures_name; ?>"><?php echo $measures_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                    </td>
                    <td><input type="text" name="purPrice<?php echo($i); ?>" value="" class="form-control" id="purPrice" placeholder="PP" /></td>
                    <td><input type="text" name="mrp<?php echo($i); ?>" value="" class="form-control" id="mrp" placeholder="MRP" /></td>
                    <td><input type="text" name="btobPrice<?php echo($i); ?>" value="" class="form-control" id="btobPrice" placeholder="B2B" /></td>
                    <td><input type="text" name="btocPrice<?php echo($i); ?>" value="" class="form-control" id="btocPrice" placeholder="B2C" /></td>
                    <td><input type="text" name="stock<?php echo($i); ?>" value="" class="form-control" id="stock" placeholder="Stock" /></td>
               	</tr>
             <?php
			 $i++;
			}
			?>
           	</tbody>
            </thead>
           </table>
            </div>
        	<div class="card-footer col-sm-12">
        		<div class="form-group">
            		<button type="submit" name="addNewProduct" class="btn btn-success pr">Submit</button>
            	</div>
        	</div>
        </div>
        <!-- Tab 2 End -->
        </form>
	</div>
</div>
