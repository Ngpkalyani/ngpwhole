<div class="card card-primary">
<form name="addBulkProductForm" action="" method="post" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Add Bulk Product</label></div>
			<div class="col-sm-3"></div>
        	<div class="col-sm-1 pr"><span class="btn btn-danger btn-sm pr" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">
        	<div class="form-group col-sm-12 row">
            	<div class="col-sm-4 pl">
                	<label class="control-label">Main Category Name *</label>
                    <select name="mainCategoryName" class="form-control select" onchange="getSubCategory(this.value);" required>
                    	<option value="<?php echo isset($_POST['mainCategoryName']) ? $_POST['mainCategoryName'] : ""; ?>" selected> --- Main Category --- <?php echo isset($_POST['mainCategoryName']) ? $_POST['mainCategoryName'] : ""; ?></option>
                        <?php
						foreach( $category_list as $category ) {
							$category_name = $category['category_name'];
							$categoryID = $category['categories_id'];
							if( $category['category_type'] == 'P' && $category['parent_id'] == '0' && $category['status'] > 0  ) {
								?>
                                <option value="<?php echo $categoryID; ?>"><?php echo $category_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                </div>
                <div class="col-sm-4 pl">
                	<label class="control-label">Sub Category Name *</label>
                    <select name="subCategoryName" class="form-control select subCategoryName" required>
                    	<option value="<?php echo isset($_POST['subCategoryName']) ? $_POST['subCategoryName'] : ""; ?>" selected> --- Sub Category --- <?php echo isset($_POST['subCategoryName']) ? $_POST['subCategoryName'] : ""; ?></option>
                    </select>
                </div>
                <div class="col-sm-4 pl">
                	<label>Brand Name </label>
                    <select name="brandName" class="form-control select">
                    	<option value="<?php echo isset($_POST['brandName']) ? $_POST['brandName'] : ""; ?>" selected> --- Brand Name --- <?php echo isset($_POST['brandName']) ? $_POST['brandName'] : ""; ?></option>
                        <?php
						foreach( $brand_list as $brand ) {
							if( $brand['status'] > 0  ) {
								$brand_name = $brand['brand_name'];
								$brandID = $brand['brands_id'];
								?>
                                <option value="<?php echo $brandID; ?>"><?php echo $brand_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                </div>				
            </div>
             
            <div class="form-group col-sm-12 row">
                <div class="col-sm-4 pl">
                	<label>Vendor Name *</label>
                    <select name="vendorName" class="form-control select" required >
                    	<option value="<?php echo isset($_POST['vendorName']) ? $_POST['vendorName'] : ""; ?>" selected> --- Vendor Name --- <?php echo isset($_POST['vendorName']) ? $_POST['vendorName'] : ""; ?></option>
                        <?php
						foreach( $vendor_list as $vendor ) {
							if( $vendor['status'] > 0  ) {
								$firm_name = $vendor['firm_name'];
								$vendorID = $vendor['vendors_id'];
								?>
                                <option value="<?php echo $vendorID; ?>"><?php echo $firm_name; ?></option>
                                <?php
							}
						}
						?>
                    </select>
                </div>
                <div class="col-sm-4 pl">
                	<label>Product File </label>
                    <input type="file" name="productListFile" required />
                </div>
                <div class="col-sm-4 pl">
                	<label>Variant File </label>
                    <input type="file" name="variantListFile" required/>
                </div>               				
			</div>
        </div>
        
        <div class="card-footer">
        	<div class="form-group">
            	<input type="reset" value="Reset" class="btn btn-danger pl" />
                <input type="submit" name="addBulkProduct" value="Submit" class="btn btn-success pr" />
            </div>
        </div>
               
	</div>
</form>
</div>
