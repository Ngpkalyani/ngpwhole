<div class="card card-primary">
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-2 pl"><span>Client List</span></div>
            <div class="col-sm-3 pl"><input class="form-control form-control-navbar light-table-filter" type="search" data-table="order-table" placeholder="Search" /></div>
            <div class="col-sm-3 pl"><span class="btn btn-info bg-addbtn" onclick="tableToExcel('uploadClientListTable', 'ClientList')">Downl Client Upload Format</span></div>
            <div class="col-sm-2 pl"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-uploadBulkClient" >Client Bulk Upload</span></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewBrand">Add New Client</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Contact No.</th>
				<th>Client Name</th>
                <th>Email ID</th>
                <th>Last Bill Date</th>
				<th>Order Count</th>
                <th>Wallet Bal.</th>
                <th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $client_list as $client )
		{
			$clientID = $client['clients_id'];
			
			$table = "wallets";
			$cond = "clients_id='$clientID'";
			$limit = "1";
			$order = '';
			$ClientWalletDetails = $Category->select($table, '', $cond, $order, $limit);
			
			$balance_amount = "";
			foreach( $ClientWalletDetails as $wallet ) {
				$balance_amount = $wallet['balance_amount'];
			}
			
			$cond = "clients_id='$clientID'";
			$order = "";
			$limit = "";
			$orderBillsCount = $Order->select('orders', '', $cond, $order, $limit);
			$billCount = 0;
			$lastBill_date = "";
			foreach( $orderBillsCount as $countBills ) {
				$billCount++;
				$lastBill_date = $countBills['order_date_time'];
			}
			
			?>
			<tr>
            	<td><?php echo($sr_no); ?></td>
				<td><?php echo $client['contact_no']; ?></td>
				<td><?php echo $client['client_name']; ?></td>
                <td><?php echo $client['email_id']; ?></td>
                <td><?php echo $lastBill_date; ?></td>
                <td><?php echo $billCount; ?></td>
                <td><?php echo $balance_amount; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<a data-href="brand_edit.php?brandID=<?php //echo $brand['brands_id']; ?>" class="modal-editBrand"><span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span></a> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
		}
			?>
		</tbody>
	</table>	
</div>
</div>

<table class="col-sm-12" id="uploadClientListTable" style="display:none">
	<thead>
    	<tr>
        	<th>Sr. No.</th>
			<th>Contact No.</th>
			<th>Client Name</th>
			<th>Email ID</th>
            <th>City</th>
            <th>Pincode</th>
			<th>Address</th>
            <th>DOB</th>
            <th>Created Date</th>
		</tr>
	</thead>
</table>

<div class="modal animate col-md-5 col-sm-12" id="modal-uploadBulkClient">
	<?php include('modules/views/client/view_bulk_client_add.php'); ?>
</div>

<div class="modal animate col-md-5 col-sm-12" id="modal-editBrand" style="height:auto">
	<div class="modal-editBrand-content">
    </div>
</div>