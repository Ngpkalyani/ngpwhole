<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Brand List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar light-table-filter" type="search" data-table="order-table" placeholder="Search" /></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewDeliverySlot">Add New Delivery Slots</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Delivery Slots</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $delivery_slot_list as $delivery_slot )
		{
			?>
			<tr>
            	<th><?php echo($sr_no); ?></th>
				<td><?php echo $delivery_slot['delivery_slots']; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<a data-href="brand_edit.php?brandID=<?php echo $brand['brands_id']; ?>" class="modal-editBrand"><span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span></a> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
		}
			?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate col-md-5 col-sm-12" id="modal-addNewDeliverySlot">
	<?php include('modules/views/delivery_slot/view_delivery_slot_add.php'); ?>
</div>
