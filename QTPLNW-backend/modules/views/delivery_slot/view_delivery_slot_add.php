<div class="card card-primary">
<form name="formAddNewDeliverySlot" id="formAddNewDeliverySlot" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create Delivery Slot</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label class="control-label">From Time *</label></div>
				<div class="col-sm-3 pl">
					<input type="text" name="fromTime" value="<?php echo isset($_POST['fromTime']) ? $_POST['fromTime'] : ""; ?>" size="2" maxlength="2" class="form-control" required />
                </div>
                <div class="col-sm-3 pl">
                    <select name="fromAMPM" class="form-control" required>
                    	<option value=""></option>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                    </select>
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label class="control-label">To Time *</label></div>
				<div class="col-sm-3 pl">
					<input type="text" name="toTime" value="<?php echo isset($_POST['toTime']) ? $_POST['toTime'] : ""; ?>" size="2" maxlength="2" class="form-control" required />
                </div>
                <div class="col-sm-3 pl">
                    <select name="toAMPM" class="form-control" required>
                    	<option value=""></option>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                    </select>
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addNewDeliverySlot" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>
