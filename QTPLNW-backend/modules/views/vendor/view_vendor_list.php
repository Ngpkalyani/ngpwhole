<div class="card card-primary" >
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Vendor List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar light-table-filter" type="search" data-table="order-table" placeholder="Search" /></div>
            <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addNewVendor">Add New Vendor</span></div>
        </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Category Name</th>
                <th>Vendor Code</th>
				<th>Firm Name</th>
				<th>Email ID</th>
                <th>Contact No.</th>
				<th>Debit Bal.</th>
				<th>Credit Bal.</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		foreach( $vendor_list as $vendor )
		{
			?>
			<tr>
            	<th><?php echo($sr_no); ?></th>
				<td><?php //echo $vendor['']; ?></td>
                <td><?php echo $vendor['vendor_code']; ?></td>
                <td><?php echo $vendor['firm_name']; ?></td>
				<td><?php echo $vendor['email_id']; ?></td>
				<td><?php echo $vendor['contact_no']; ?></td>
				<td><?php //echo $category['sort_order']; ?></td>
				<td><?php //echo $categoryImages; ?></td>
				<td>
					<label class="btn-active mb0">
						<input type="checkbox" name="status" />
						<span class="btn-active-onoff round"></span>
					</label>
				</td>
				<td>
					<span class="btn-info btn-view"><i class="fa fa-eye"></i></span> &nbsp;
					<span class="btn-info btn-edit"><i class="fa fa-pencil-alt"></i></span> &nbsp;
					<span class="btn-info btn-delete"><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
			<?php
			$sr_no++;
		}
			?>
		</tbody>
	</table>	
</div>
</div>

<div class="modal animate col-md-5 col-sm-12" id="modal-addNewVendor">
	<?php include('modules/views/vendor/view_vendor_add.php'); ?>
</div>
