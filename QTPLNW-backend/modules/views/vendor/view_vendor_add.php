<div class="card card-primary">
	<div class="card-header col-sm-12">
    	<div class="col-sm-8 pl"><label>Create New Vendor</label></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
    </div>
    <div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">
    	<div class="stepwizard col-md-offset-3">
        	<div class="stepwizard-row setup-panel">
            	<div class="stepwizard-step"><a href="#step-1" type="button" class="btn btn-primary">Personal Info.</a></div>
                <div class="stepwizard-step"><a href="#step-2" type="button" class="btn btn-default" disabled="disabled">Bank Details</a></div>
                <div class="stepwizard-step"><a href="#step-3" type="button" class="btn btn-default" disabled="disabled">Login Details</a></div>
            </div>
        </div>    
    </div>
    <form name="addNewVendorForm" action="" method="post" enctype="multipart/form-data">
    <div class="row setup-content" id="step-1">
    	<div class="form-group col-sm-12 clearfix container-fluid">
        	<div class="col-sm-4 pl"><label class="control-label">Firm Name *</label></div>
            <div class="col-sm-7 pl">
            	<input type="text" name="firmName" class="form-control" required placeholder="Firm Name" />
            </div>
        </div>
        <div class="form-group col-sm-12 clearfix container-fluid">
        	<div class="col-sm-4 pl"><label class="control-label">Proprietor Name: *</label></div>
            <div class="col-sm-7 pl">
            	<input type="text" name="proprietorName" class="form-control" required placeholder="Proprietor Name" />
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-4 pl"><label class="control-label">Contact No: *</label></div>
            <div class="col-sm-7 pl">
            	<input type="number" name="contactNo" maxlength="10" class="form-control col-sm-6" required placeholder="Contact No." />
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-4 pl"><label class="control-label">Address : *</label></div>
            <div class="col-sm-7 pl">
            	<textarea name="address" class="form-control" placeholder="Address" required></textarea>
            </div>
        </div>
        <div class="form-group col-sm-12">
        	<div class="col-sm-6 pl">
            	<label class="control-label">GST No</label>
                <input type="text" name="gstNo" maxlength="15" class="form-control" placeholder="GST No." />
            </div>
            <div class="col-sm-6 pl">
            	<label class="control-label">PAN No</label>
                <input type="text" name="panNo" maxlength="10" class="form-control" placeholder="PAN No." />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="form-group">
            	<button class="btn btn-primary nextBtn pr" type="button" >Next</button>
            </div>
        </div>
    </div>
    
    <div class="row setup-content" id="step-2">
    	<div class="form-group col-sm-12">
       		<div class="col-sm-4 pl"><label class="control-label">Bank Name</label></div>
            <div class="col-sm-7 pl">
            	<input type="text" name="bankName" class="form-control" placeholder="Bank Name" />
            </div>
        </div>
        <div class="form-group col-sm-12">
       		<div class="col-sm-4 pl"><label class="control-label">Bank A/c No.</label></div>
            <div class="col-sm-7 pl">
            	<input type="number" name="bankAcNo" class="form-control" placeholder="Bank A/c No." />
            </div>
        </div>
        <div class="form-group col-sm-12">
       		<div class="col-sm-4 pl"><label class="control-label">IFSC Code</label></div>
            <div class="col-sm-7 pl">
            	<input type="text" name="ifscCode" class="form-control" placeholder="IFSC Code" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="form-group">
            	<button class="btn btn-primary nextBtn pr" type="button" >Next</button>
            </div>
        </div>
    </div>
    
    <div class="row setup-content" id="step-3">
    	<div class="card-footer col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Email ID :</label></div>
            <div class="col-sm-8 pl">
            	<input type="email" name="emailID" class="form-control" required placeholder="Email ID" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Username :</label></div>
            <div class="col-sm-8 pl">
            	<input type="text" name="username" class="form-control" required maxlength="10" placeholder="Username" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="col-sm-3 pl"><label class="control-label">Password :</label></div>
            <div class="col-sm-8 pl">
            	<input type="password" name="password" class="form-control" required placeholder="Password" />
            </div>
        </div>
        <div class="card-footer col-sm-12">
        	<div class="form-group">
            	<button type="submit" name="addNewVendor" class="btn btn-success pr">Submit</button>
            </div>
        </div>
    </div>
    
    </form>
</div>