<div class="card card-primary">
<form name="formAddNewSection" id="formAddNewSection" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create New Section</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label class="control-label">Section Name *</label></div>
				<div class="col-sm-8 pl">
					<input type="text" name="sectionName" value="<?php echo isset($_POST['sectionName']) ? $_POST['sectionName'] : ""; ?>" required placeholder="Section Name" class="form-control" />
				</div>
			</div>            
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Sort Order *</label></div>
				<div class="col-sm-3 pl">
					<input type="number" name="sortOrder" value="<?php echo isset($_POST['sortOrder']) ? $_POST['sortOrder'] : ""; ?>" required class="form-control" maxlength="2" />
				</div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-4 pl"><label>Product List</label></div>
				<div class="col-sm-8 pl">
					<input type="text" name="productListName" value="" data-role="tagsinput" />
				</div>
			</div>
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addNewSection" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>

