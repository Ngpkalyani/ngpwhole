<div class="row">
	<div class="form-group col-sm-12">
    <form name="srchOrderRptForm" action="" method="POST" enctype="multipart/form-data">
    	<div class="col-sm-2 pl">
        	<input type="text" name="contactNo" placeholder="Contact No" size="" maxlength="10" value="" class="form-control" />
        </div>
        <div class="col-sm-2 pl">
        	<input type="text" name="startDate" placeholder="Start Date" size="16" value="" class="form-control" id="startDate" />        	
        </div>
        <div class="col-sm-2 pl">
        	<input type="text" name="endDate" placeholder="End Date" size="16" value="" class="form-control" id="endDate" />
        </div>
        <div class="col-sm-2 pl">
        	<select name="orderType" class="form-control">
            	<option value=""></option>
                <option value="B2B">B2B</option>
                <option value="B2C">B2C</option>
            </select>
        </div>
        <div class="col-sm-2 pl">
        	<select name="paymentMode" class="form-control">
            	<option value=""></option>
                <option value="COD">COD</option>
                <option value="Online Payment">Online Payment</option>
            </select>
        </div>
        <div class="col-sm-1 pr">
        	<input type="submit" name="SearchOrderReport" value="Submit" class="btn btn-success pr" />
        </div>
        </form>
    </div>
</div>
<?php
$orderDetails = "";
if( isset($_POST['SearchOrderReport']) )
{
	$contactNo = isset($_POST['contactNo']) ? $_POST['contactNo'] : "";
	$orderType = isset($_POST['orderType']) ? $_POST['orderType'] : "";
	$paymentMode = isset($_POST['paymentMode']) ? $_POST['paymentMode'] : "";
	$fromDate = isset($_POST['startDate']) ? $_POST['startDate'] : "";
	$toDate = isset($_POST['endDate']) ? $_POST['endDate'] : "";
	$startDate = "";
	$endDate = "";
	if( !empty($fromDate) && !empty($toDate) ) {
		$startDate = explode('-',$fromDate);
		$startDate = $startDate[2]."-".$startDate[1]."-".$startDate[0];
		$endDate = explode('-',$toDate);
		$endDate = $endDate[2]."-".$endDate[1]."-".$endDate[0];
	}
	
	$orderTable = "orders";
	$clientsTable = "clients";
	if( !empty($contactNo) ) {
		$cond = "contact_no='$contactNo'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);
	}
	if( !empty($orderType) ) {
		$cond = "order_type='$orderType'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);
	}
	if( !empty($paymentMode) ) {
		$paymentMode = $paymentMode == "COD" ? "1" : "2";
		$cond = "payment_mode='$paymentMode'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);	
	}
	if( !empty($startDate) && !empty($endDate) ) {
		$cond = "order_date BETWEEN '$startDate' AND '$endDate'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);
	}		
	if( !empty($startDate) && !empty($endDate) && !empty($orderType) ) {
		$cond = "order_date BETWEEN '$startDate' AND '$endDate' AND order_type='$orderType'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);	
	}
	if( !empty($startDate) && !empty($endDate) && !empty($paymentMode) ) {
		$paymentMode = $paymentMode == "COD" ? "1" : "2";
		$cond = "order_date BETWEEN '$startDate' AND '$endDate' AND payment_mode='$paymentMode'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);	
	}
	if( !empty($startDate) && !empty($endDate) && !empty($orderType) && !empty($paymentMode) ) {
		$paymentMode = $paymentMode == "COD" ? "1" : "2";
		$cond = "order_date BETWEEN '$startDate' AND '$endDate' AND order_type='$orderType' AND payment_mode='$paymentMode'";
		$order = "order_date DESC";
		$orderDetails = $Order->select($orderTable, '', $cond, $order);	
	}
	
}
else 
{
		$todaysData = date('Y-m-d');
		$cond = "order_date='$todaysData'";
		$order = "order_date DESC";
		$orderDetails = $Order->select('orders', '', $cond, $order);

}
?>

<div class="card card-primary">
    <div class="card-header">
    	<div class="col-sm-12">
        	<div class="col-sm-3 pl"><span>Order List</span></div>
            <div class="col-sm-5 pl"><input class="form-control form-control-navbar light-table-filter" type="search" placeholder="Search" data-table="order-table" aria-label="Search"></div>
            <div class="col-sm-2 pl"><a href="order_create.php"><span class="btn btn-info bg-addbtn" >Create Order</span></a></div>
        </div>
        <div class="col-sm-2 pr"><span class="btn btn-info bg-addbtn" data-toggle="modal" data-target="#modal-addAutoOrder">Create Auto Order</span></div>
    </div>
	<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped order-table">
		<thead>
			<tr>
				<th>#</th>
                <th>Customer Name & No</th>
				<!--<th>Address</th>-->
				<th>Payment Mode & Amt</th>
                <th>Order Time & Deli. Slots</th>
				<th>Order Status & Remark</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$sr_no = 1;
		$total = 0;
		foreach( $orderDetails as $orders )
		{
			$clients_id = $orders['clients_id'];
			$payment_mode = "COD";
			if( $orders['payment_mode'] == 2 ) {
				$payment_mode = "Online Payment";
			}
			
			$sql = "SELECT * FROM clients WHERE clients_id='$clients_id'";
			$res = $Category->run($sql);
			$res_category = mysqli_fetch_assoc($res);
			$client_name = strtolower($res_category['client_name']);
			$client_name = ucwords($client_name);
			$address = $res_category['address'];
			
			$orderStatus = $orders['cilent_order_status'];
			/*	For User Status		*/
		  	switch ($orderStatus)
			{
				case "2":
				$order_status = 'Inprocess';
				break;
				
				case "3":
				$order_status = 'Ready to Dispatch';
				break;
				
				case "4":
				$order_status = 'Delivered';
				break;
				
				case "5":
				$order_status = 'Success';
				break;
				
				case "6":
				$order_status = 'Decline';
				break;
				
				default:
				$order_status = 'Pending';				
			}
			
			?>
			<tr>
				<td><?php echo($sr_no++); ?></td>
                <td><?php echo $client_name." - ".$orders['contact_no']; ?></td>
				<!--<td><?php //echo $address; ?></td>-->
				<td><?php echo $payment_mode." - Rs.".$orders['final_total']; ?></td>
                <td><?php echo $orders['order_date_time']; ?></td>
				<td><?php echo $order_status; ?>
					<!--<label class="btn-active mb0">
						<select>
                        	<option></option>
                        </select>
					</label>-->
				</td>
				<td>
					<a data-href="../QTPLNW-backend/order_bill.php?orderID=<?php echo $orders['orders_id']; ?>" class="modal-viewOrder"><span class="btn-info btn-view"><i class="fa fa-eye"></i></span></a> &nbsp;
                    <span class="btn-info btn-delete" data-toggle="modal" data-target="#modal-deleteOrder" data-id="<?php echo $orders['orders_id']; ?>" ><i class="fa fa-trash-alt"></i></span> &nbsp;
				</td>
			</tr>
        <?php
		$total = $total + $orders['final_total'];
		}
		?>
		</tbody>
        <tfoot>
        	<tr>
				<th>#</th>
                <th>Total</th>
				<!--<th>Address</th>-->
				<th>Rs. <?php echo $total; ?></th>
                <th>Order Time & Deli. Slots</th>
				<th>Order Status & Remark</th>
				<th>Action</th>
			</tr>
        </tfoot>
	</table>	
	</div>
</div>
<?php
//}
?>

<div class="modal animate col-md-8 col-sm-12" id="modal-addAutoOrder">
	<?php include('modules/views/order/view_auto_order_add.php'); ?>
</div>

<div class="modal animate col-md-7 col-sm-12" id="modal-viewOrder" style="height:auto">
	<div class="modal-viewOrder-content">
    </div>
</div>

<div class="modal animate col-md-4 col-sm-12" id="modal-deleteOrder" style="height:auto">
	<div class="modal-deleteOrder-content">
    </div>
</div>

<div style="display:none">
<form name="updateOrder" action="" method="POST" enctype="multipart/form-data">
	<input type="file" name="updateOrderSheet" required />
    <input type="submit" name="updateOrder" value="Submit" class="btn btn-success pr" />
</form>
</div>

