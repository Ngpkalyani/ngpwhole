<div class="card card-primary">
<form name="formAddAutoOrder" id="formAddAutoOrder" method="post" action="" enctype="multipart/form-data">
	<div class="form-report">
		<!-- Form Header -->
		<div class="card-header col-sm-12">
			<div  class="col-sm-8 pl"><label>Create Auto Order</label></div>
			<div class="col-sm-3"></div>
            <div class="col-sm-1 pr"><span class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></span></div>
		</div>
		<!-- #End Form Header -->
		
		<!-- Form Body -->
		<div class="modal-body" style="height:auto; max-height:calc(100vh - 85px); overflow-y:auto;">					
			<div class="form-group col-sm-12 row">
				<div class="col-sm-3 pl">
                	<label>Order Type *</label>
                    <select name="orderType" class="form-control" required>
                    	<option value=""></option>
                        <option value="B2B">B2B</option>
                        <option value="B2C">B2C</option>
                    </select>
                </div>
                <div class="col-sm-3 pl">
                	<label>Shipping Charges *</label>
                    <input type="number" name="shippingCharge" value="" required placeholder="Shipping Amt" maxlength="3" class="form-control" />
                </div>
			</div>
            <div class="form-group col-sm-12 row">
				<div class="col-sm-3 pl">
                	<label>Order Date-1 *</label>
                    <div class="input-append date form_datetime" data-date="">
                    	<input type="text" name="dateTime1" size="16" value="" class="form-control" required >
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
                <div class="col-sm-3 pl">
                	<label>Order Date-2</label>
                    <div class="input-append date form_datetime" data-date="">
                    	<input type="text" name="dateTime2" size="16" value="" class="form-control" >
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
                <div class="col-sm-3 pl">
                	<label>Order Date-3</label>
                    <div class="input-append date form_datetime" data-date="">
                    	<input type="text" name="dateTime3" size="16" value="" class="form-control"  >
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
                <div class="col-sm-3 pl">
                	<label>Order Date-4</label>
                    <div class="input-append date form_datetime" data-date="">
                    	<input type="text" name="dateTime4" size="16" value="" class="form-control" >
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
			</div>
            
            <div class="form-group col-sm-12 row">
            	<div class="col-sm-3 pl">
                	<label>Temp 1 *</label>
                	<input type="file" name="temp1" required />
                </div>
                <div class="col-sm-3 pl">
                	<label>Temp 2</label>
                	<input type="file" name="temp2" />
                </div>
                <div class="col-sm-3 pl">
                	<label>Temp 3</label>
                	<input type="file" name="temp3" />
                </div>
                <div class="col-sm-3 pl">
                	<label>Temp 4</label>
                	<input type="file" name="temp4" />
                </div>            
            </div>            
		</div>
		<!-- #End Form Body -->
        <!-- Form Footer -->
		<!--<div class="modal-footer">-->
			<div class="card-footer">
				<div class="form-group">
	            	<input type="reset" value="Reset" class="btn btn-danger pl" />
					<input type="submit" name="addAutoOrder" value="Submit" class="btn btn-success pr" />
				</div>
			</div>
		<!--</div>-->
        <!-- #End Form Footer -->
</div>
</form>
</div>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy - HH:ii P",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
</script>
